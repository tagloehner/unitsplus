import { combineReducers } from 'redux'
import { theme } from './theme'
import { language } from './language'
import { list } from './list'
import { config } from './config'
import { numpad } from './numpad'
import { scrollBar } from './scrollBar'
import { toast } from './toast'
import { decimals } from './decimals'

export const combinedReducers = combineReducers({
  language,
  theme,
  list,
  config,
  numpad,
  scrollBar,
  toast,
  decimals
})
