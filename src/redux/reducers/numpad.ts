import {
  ON_NUMBER_PRESSED,
  ON_BACKSPACE_PRESSED,
  ON_ALL_CLEAR_PRESSED,
  ON_MINUS_PLUS_PRESSED,
  ON_DOT_PRESSED,
  SET_VALUE,
} from '../types'
import { AREA } from '../../constants'
import { switchCase } from '../../functions'


const INITIAL_STATE = {
  area: { value: '0' },
  data: { value: '0' },
  fuel: { value: '0' },
  length: { value: '0' },
  power: { value: '0' },
  pressure: { value: '0' },
  speed: { value: '0' },
  temperature: { value: '0' },
  time: { value: '0' },
  volume: { value: '0' },
  weight: { value: '0' },
}

export const numpad = (state = INITIAL_STATE, action) => {
  const { payload, type } = action

  const category = payload && payload.category || AREA
  const input = payload && payload.input || 0
  const stateValue = state[category].value

  return switchCase({
    [ON_NUMBER_PRESSED]: {
      ...state,
      [category]: {
        value: input === 0 && stateValue === '0'
          ? stateValue
          : input !== 0 && stateValue === '0'
            ? `${input}`
            : stateValue + input,
      },
    },
    [ON_BACKSPACE_PRESSED]: {
      ...state,
      [category]: {
        value: stateValue.length === 1
          ? '0'
          : stateValue.length === 2 && stateValue.includes('-')
            ? '0'
            : stateValue.slice(0, -1),
      },
    },
    [ON_ALL_CLEAR_PRESSED]: {
      ...state,
      [category]: {
        value: '0',
      },
    },
    [ON_MINUS_PLUS_PRESSED]: {
      ...state,
      [category]: {
        value: (stateValue * -1).toString(),
      },
    },
    [ON_DOT_PRESSED]: {
      ...state,
      [category]: {
        value: stateValue.length > 0 && (input && !stateValue.includes(input))
          ? stateValue + input
          : stateValue,
      },
    },
    [SET_VALUE]: {
      ...state,
      [category]: {
        value: input,
      },
    },
  })(state)(type)
}
