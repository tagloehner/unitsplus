import { SET_LANGUAGE_PORTUGUESE, SET_LANGUAGE_ENGLISH } from '../types'
import { switchCase } from '../../functions'
import { english } from '../../i18n'


const INITIAL_STATE = {
  ...english
}

export const language = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  return switchCase({
    [SET_LANGUAGE_PORTUGUESE]: { ...payload },
    [SET_LANGUAGE_ENGLISH]: { ...payload }
  })(state)(type)
}
