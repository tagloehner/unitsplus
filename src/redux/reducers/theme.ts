import { SET_THEME_LIGHT, SET_THEME_DARK } from '../types'
import { switchCase } from '../../functions'
import { dark } from '../../themes'

const INITIAL_STATE = {
  ...dark
}

export const theme = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  return switchCase({
    [SET_THEME_LIGHT]: { ...payload },
    [SET_THEME_DARK]: { ...payload }
  })(state)(type)
}
