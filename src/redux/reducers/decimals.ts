import { SET_DECIMALS_ENABLED, SET_DECIMALS_DISABLED } from '../types'
import { switchCase } from '../../functions'


const INITIAL_STATE = {
  isDecimalsEnabled: true
}

export const decimals = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  return switchCase({
    [SET_DECIMALS_ENABLED]: { isDecimalsEnabled: payload },
    [SET_DECIMALS_DISABLED]: { isDecimalsEnabled: payload },
  })(state)(type)
}
