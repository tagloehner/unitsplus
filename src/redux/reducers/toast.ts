import { CALL_TOAST } from '../types'
import { switchCase, generateRandomString } from '../../functions'


const INITIAL_STATE = {
  message: undefined,
  duration: 800,
  id: undefined,
}

export const toast = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  return switchCase({
    [CALL_TOAST]: { ...state, ...payload, id: generateRandomString() },
  })(state)(type)
}
