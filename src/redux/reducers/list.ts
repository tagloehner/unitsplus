import {
  ON_SELECTED_ITEM_UPDATE_LIST_LEFT,
  ON_SELECTED_ITEM_UPDATE_LIST_RIGHT,
} from '../types'
import * as CONST from '../../constants'
import { switchCase } from '../../functions'


const INITIAL_STATE = {
  area: {
    leftOperation: CONST.HECTARE,
    rightOperation: CONST.HECTARE,
    leftIndex: 0,
    rightIndex: 0,
  },
  data: {
    leftOperation: CONST.BIT,
    rightOperation: CONST.BIT,
    leftIndex: 0,
    rightIndex: 0,
  },
  fuel: {
    leftOperation: CONST.MILE,
    rightOperation: CONST.MILE,
    leftIndex: 0,
    rightIndex: 0,
  },
  length: {
    leftOperation: CONST.KILOMETER,
    rightOperation: CONST.KILOMETER,
    leftIndex: 0,
    rightIndex: 0,
  },
  power: {
    leftOperation: CONST.BTU_H,
    rightOperation: CONST.BTU_H,
    leftIndex: 0,
    rightIndex: 0,
  },
  pressure: {
    leftOperation: CONST.ATMOSPHERE,
    rightOperation: CONST.ATMOSPHERE,
    leftIndex: 0,
    rightIndex: 0,
  },
  speed: {
    leftOperation: CONST.KILOMETER_PER_HOUR,
    rightOperation: CONST.KILOMETER_PER_HOUR,
    leftIndex: 0,
    rightIndex: 0,
  },
  temperature: {
    leftOperation: CONST.CELSIUS,
    rightOperation: CONST.CELSIUS,
    leftIndex: 0,
    rightIndex: 0,
  },
  time: {
    leftOperation: CONST.MILLENNIUM,
    rightOperation: CONST.MILLENNIUM,
    leftIndex: 0,
    rightIndex: 0,
  },
  volume: {
    leftOperation: CONST.CUBIC_METER,
    rightOperation: CONST.CUBIC_METER,
    leftIndex: 0,
    rightIndex: 0,
  },
  weight: {
    leftOperation: CONST.KILOGRAM,
    rightOperation: CONST.KILOGRAM,
    leftIndex: 0,
    rightIndex: 0,
  },
}

export const list = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  // initialize to prevent crash
  const category = payload && payload.category || CONST.AREA
  const operation = payload && payload.operation || ''
  const index = payload && payload.index || 0

  return switchCase({
    [ON_SELECTED_ITEM_UPDATE_LIST_LEFT]: {
      ...state,
      [category]: { ...state[category], leftOperation: operation, leftIndex: index },
    },
    [ON_SELECTED_ITEM_UPDATE_LIST_RIGHT]: {
      ...state,
      [category]: { ...state[category], rightOperation: operation, rightIndex: index }
    },
  })(state)(type)
}
