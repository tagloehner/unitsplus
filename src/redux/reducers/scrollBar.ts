import { ON_SCROLL_BAR_BUTTON_PRESSED } from '../types'
import { AREA } from '../../constants'
import { switchCase } from '../../functions'


const INITIAL_STATE = {
  category: AREA, index: 0,
}

export const scrollBar = (state = INITIAL_STATE, action) => {
  const { payload } = action
  return switchCase({
    [ON_SCROLL_BAR_BUTTON_PRESSED]: { ...state, ...payload },
  })(state)(action.type)
}
