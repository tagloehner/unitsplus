import { SET_COMPONENTS_HEIGHT } from '../types'
import { switchCase } from '../../functions'


const INITIAL_STATE = {
  listHeight: 0,
  listItemHeight: 0,
  numpadItemHeight: 0,
  scrollBarHeight: 0
}

export const config = (state = INITIAL_STATE, action) => {
  const { payload, type } = action
  return switchCase({
    [SET_COMPONENTS_HEIGHT]: { ...state, ...payload }
  })(state)(type)
}
