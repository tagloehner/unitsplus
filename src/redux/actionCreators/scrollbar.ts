import { ON_SCROLL_BAR_BUTTON_PRESSED } from '../types'
import { Dispatch } from 'redux'


export const onScrollBarButtonPressed = payload => (dispatch: Dispatch) => dispatch({ type: ON_SCROLL_BAR_BUTTON_PRESSED, payload })
