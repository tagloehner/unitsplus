import { Platform, Dimensions } from 'react-native'
import DeviceInfo from 'react-native-device-info'
import { SET_COMPONENTS_HEIGHT } from '../types'
import { Dispatch } from 'redux'


let hasNotch = DeviceInfo.hasNotch()
const { height, width } = Dimensions.get('screen')

import {
  SET_LIST_ITEM_HEIGHT,
} from '../types'

export const setListItemHeight = (payload: number) => (dispatch: Dispatch) => {
  dispatch({ type: SET_LIST_ITEM_HEIGHT, payload })
}

export const setComponentsHeight = () => (dispatch: Dispatch) => {
  const numpadItemHeight = (width / 6) - (hasNotch ? 6 : 3)
  const numpadHeight = numpadItemHeight * 4
  const scrollBarHeight = (width / 8) - (hasNotch ? 6 : 3)
  const numberOfRows = height >= 812 ? 7 : 5
  let total: number

  // TODO: Add Android Support
  if (Platform.OS === 'ios')
  {
    total = numpadHeight + scrollBarHeight + (hasNotch ? 78 : 20)
  }

  dispatch({
    type: SET_COMPONENTS_HEIGHT,
    payload: {
      listHeight: Math.round((height - total)),
      listItemHeight: Math.round((height - total) / numberOfRows),
      numpadItemHeight: Math.round(numpadItemHeight),
      scrollBarHeight
    }
  })
}
