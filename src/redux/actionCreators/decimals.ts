import { SET_DECIMALS_ENABLED, SET_DECIMALS_DISABLED } from '../types'
import { switchCase } from '../../functions'
import { Dispatch } from 'redux'

export const setDecimalsEnabled = (isDecimalsEnabled: boolean) => (dispatch: Dispatch) => switchCase({
  true: () => dispatch({ type: SET_DECIMALS_ENABLED, payload: true }),
  false: () => dispatch({ type: SET_DECIMALS_DISABLED, payload: false }),
})(true)(isDecimalsEnabled)

