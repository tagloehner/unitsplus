import { switchCase } from '../../functions'
import { SET_LANGUAGE_ENGLISH, SET_LANGUAGE_PORTUGUESE } from '../types'
import { ENGLISH, PORTUGUESE } from '../../constants'
import { english, portuguese } from '../../i18n'
import { Dispatch } from 'redux'


export const setLanguage = (language: string) => (dispatch: Dispatch) => switchCase({
  [ENGLISH]: () => dispatch({ type: SET_LANGUAGE_ENGLISH, payload: english }),
  [PORTUGUESE]: () => dispatch({ type: SET_LANGUAGE_PORTUGUESE, payload: portuguese }),
})(ENGLISH)(language)
