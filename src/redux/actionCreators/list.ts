import {
  ON_SELECTED_ITEM_UPDATE_LIST_LEFT,
  ON_SELECTED_ITEM_UPDATE_LIST_RIGHT,
} from '../types'
import { LIST_LEFT, LIST_RIGHT } from '../../constants'
import { switchCase } from '../../functions'


export const onSelectedItemUpdate = payload => dispatch => {
  const { listIdentifier } = payload
  return switchCase({
    [LIST_LEFT]: () => dispatch({ type: ON_SELECTED_ITEM_UPDATE_LIST_LEFT, payload }),
    [LIST_RIGHT]: () => dispatch({ type: ON_SELECTED_ITEM_UPDATE_LIST_RIGHT, payload }),
  })(undefined)(listIdentifier)
}
