import {
  ON_NUMBER_PRESSED,
  ON_BACKSPACE_PRESSED,
  ON_ALL_CLEAR_PRESSED,
  ON_MINUS_PLUS_PRESSED,
  ON_DOT_PRESSED,
  SET_VALUE,
} from '../types'
import { Dispatch } from 'redux'


export const onNumberPressed = (payload: number) => (dispatch: Dispatch) => dispatch({ type: ON_NUMBER_PRESSED, payload })
export const onBackspacePressed = (payload: any) => (dispatch: Dispatch) => dispatch({ type: ON_BACKSPACE_PRESSED, payload })
export const onAllClearPressed = (payload: any) => (dispatch: Dispatch) => dispatch({ type: ON_ALL_CLEAR_PRESSED, payload })
export const onMinusPLusPress = (payload: any) => (dispatch: Dispatch) => dispatch({ type: ON_MINUS_PLUS_PRESSED, payload })
export const onDotPressed = (payload: any) => (dispatch: Dispatch) => dispatch({ type: ON_DOT_PRESSED, payload })
export const setValue = (payload: any) => (dispatch: Dispatch) => dispatch({ type: SET_VALUE, payload })
