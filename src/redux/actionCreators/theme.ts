import { SET_THEME_LIGHT, SET_THEME_DARK } from '../types'
import { switchCase } from '../../functions'
import { Dispatch } from 'redux'
import { dark, light } from '../../themes'


export const setThemeDark = (isThemeDark: boolean) => (dispatch: Dispatch) => switchCase({
  false: () => dispatch({ type: SET_THEME_LIGHT, payload: light }),
  true: () => dispatch({ type: SET_THEME_DARK, payload: dark }),
})(true)(isThemeDark)
