import { CALL_TOAST } from '../types'
import { Dispatch } from 'redux'


export const callToast = payload => (dispatch: Dispatch) => {
  dispatch({ type: CALL_TOAST, payload })
}
