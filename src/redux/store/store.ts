import { applyMiddleware, createStore, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
import AsyncStorage from '@react-native-community/async-storage'
import { combinedReducers } from '../reducers'


const persistConfig = { key: 'Units Plus', storage: AsyncStorage, whitelist: ['config', 'theme', 'language'] }
const persistedReducer = persistReducer(persistConfig, combinedReducers)
const middlewares = [applyMiddleware(thunk)]

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(persistedReducer, {}, composeEnhancers(...middlewares))
export const persistor = persistStore(store)
