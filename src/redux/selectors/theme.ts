import { createSelector } from 'reselect'

const getTheme = state => state.theme

export const statusBarStyles = createSelector(getTheme, theme => theme.statusBar)
export const safeAreaViewStyles = createSelector(getTheme, theme => theme.safeAreaView)
export const listStyles = createSelector(getTheme, theme => theme.list)
export const converterStyles = createSelector(getTheme, theme => theme.converter)
export const headerStyles = createSelector(getTheme, theme => theme.header)
export const scrollBarStyles = createSelector(getTheme, theme => theme.scrollBar)
export const numpadStyles = createSelector(getTheme, theme => theme.numpad)
export const displayStyles = createSelector(getTheme, theme => theme.display)
export const tabBarStyles = createSelector(getTheme, theme => theme.tabBar)
export const buttonStyles = createSelector(getTheme, theme => theme.button)
export const toastStyles = createSelector(getTheme, theme => theme.toast)
export const gradientStyles = createSelector(getTheme, theme => theme.gradient)
export const preferencesStyles = createSelector(getTheme, theme => theme.preferences)
export const getThemeStyle = createSelector(getTheme, theme => theme.config.theme)
