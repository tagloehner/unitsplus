import { createSelector } from 'reselect'

const getDecimals = state => state.decimals

export const getIsDecimalsEnabled = createSelector(getDecimals, decimals => decimals.isDecimalsEnabled)