import { createSelector } from 'reselect'

const getLanguage = state => state.language

export const i18nCategories = createSelector(getLanguage, language => language.categories)
export const i18nOperations = createSelector(getLanguage, language => language.operations)
export const i18nPrefixes = createSelector(getLanguage, language => language.prefixes)
export const i18nNumpad = createSelector(getLanguage, language => language.numpad)
export const i18nPreferences = createSelector(getLanguage, language => language.preferences)
export const i18nLanguage = createSelector(getLanguage, language => language.config)
