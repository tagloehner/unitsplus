import { createSelector } from 'reselect'
import { switchCase } from '../../functions'
import {
  calculateArea,
  calculateData,
  calculateFuel,
  calculateLength,
  calculatePower,
  calculatePressure,
  calculateSpeed,
  calculateTemperature,
  calculateTime,
  calculateVolume,
  calculateWeight,
} from '../../operations'
import * as TYPE from '../../constants'


const getSelectedCategory = state => state.scrollBar.category
const getSelectedOperation = (state, props) => state.list[props.category]
const getNumpadValue = (state, props) => state.numpad[props.category]

export const makeGetCalculation = () => createSelector(
  [getSelectedCategory, getSelectedOperation, getNumpadValue],
  (selectedCategory, selectedOperation, numpadValue) => {
    const { leftOperation, rightOperation, leftIndex, rightIndex } = selectedOperation
    const { value } = numpadValue

    const result = switchCase({
      [TYPE.AREA]: () => calculateArea({ value, leftOperation, rightOperation }),
      [TYPE.DATA]: () => calculateData({ value, leftOperation, rightOperation }),
      [TYPE.FUEL]: () => calculateFuel({ value, leftOperation, rightOperation }),
      [TYPE.LENGTH]: () => calculateLength({ value, leftOperation, rightOperation }),
      [TYPE.POWER]: () => calculatePower({ value, leftOperation, rightOperation }),
      [TYPE.PRESSURE]: () => calculatePressure({ value, leftOperation, rightOperation }),
      [TYPE.SPEED]: () => calculateSpeed({ value, leftOperation, rightOperation }),
      [TYPE.TEMPERATURE]: () => calculateTemperature({ value, leftOperation, rightOperation }),
      [TYPE.TIME]: () => calculateTime({ value, leftOperation, rightOperation }),
      [TYPE.VOLUME]: () => calculateVolume({ value, leftOperation, rightOperation }),
      [TYPE.WEIGHT]: () => calculateWeight({ value, leftOperation, rightOperation }),
    })(undefined)(selectedCategory)

    return { value, leftOperation, rightOperation, result, leftIndex, rightIndex }
  },
)
