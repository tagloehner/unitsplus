import { createSelector } from 'reselect'

const getConfig = state => state.config

export const listItemHeight = createSelector(getConfig, config => config.listItemHeight)
export const numpadItemHeight = createSelector(getConfig, config => config.numpadItemHeight)
export const scrollBarHeight = createSelector(getConfig, config => config.scrollBarHeight)
export const listHeight = createSelector(getConfig, config => config.listHeight)