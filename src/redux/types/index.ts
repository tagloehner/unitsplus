export const SET_THEME_LIGHT = 'theme/setThemeLight'
export const SET_THEME_DARK = 'theme/setThemeDark'

export const ON_NUMBER_PRESSED = 'numpad/onNumberPressed'
export const ON_BACKSPACE_PRESSED = 'numpad/onBackspacePressed'
export const ON_ALL_CLEAR_PRESSED = 'numpad/onAllClearPressed'
export const ON_MINUS_PLUS_PRESSED = 'numpad/onMinusPlusPressed'
export const ON_DOT_PRESSED = 'numpad/onDotPressed'
export const SET_VALUE = 'numpad/setValue'

export const ON_SELECTED_ITEM_UPDATE_LIST_LEFT =
  'list/onSelectedItemUpdateListLeft'
export const ON_SELECTED_ITEM_UPDATE_LIST_RIGHT =
  'list/onSelectedItemUpdateListRight'

export const SET_COMPONENTS_HEIGHT = 'config/setComponentsHeight'
export const SET_LIST_ITEM_HEIGHT = 'config/setListItemHeight'

export const ON_SCROLL_BAR_BUTTON_PRESSED = 'scrollBar/onScrollBarButtonPressed'

export const CALL_TOAST = 'toast/callToast'

export const SET_LANGUAGE_PORTUGUESE = 'preferences/setLanguagePortuguese'
export const SET_LANGUAGE_ENGLISH = 'preferences/setLanguageEnglish'

export const SET_DECIMALS_ENABLED = 'preferences/setDecimalsEnabled'
export const SET_DECIMALS_DISABLED = 'preferences/setDecimalsDisabled'