import React from 'react'
import { AppNavigator, NavigationService } from '../navigator'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from '../redux/store'
import { SafeAreaView, StatusBar } from '../components'


export const Core: React.FC = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <StatusBar />
      <SafeAreaView>
        <AppNavigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef)
          }}
        />
      </SafeAreaView>
    </PersistGate>
  </Provider>
)
