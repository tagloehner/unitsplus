import { Clipboard } from 'react-native'
import { operationsList } from '../models'

// @ts-ignore
export const isMobile = window.innerWidth < 480

const ifFunction = f => (f instanceof Function ? f() : f)
// eslint-disable-next-line no-prototype-builtins
export const switchCaseHelper = cases => defaultCase => key =>
  cases.hasOwnProperty(key) ? cases[key] : defaultCase

export const switchCase = cases => defaultCase => key =>
  ifFunction(switchCaseHelper(cases)(defaultCase)(key))

export const generateRandomString = () => Math.random().toString(36).substring(7)


const localeStringOptions = { style: 'decimal', maximumFractionDigits: 20 }
export const toLocaleString = string => Number(string).toLocaleString(undefined, localeStringOptions)


export const findSelectedItemIndex = (category, operationName) => {
  const getCategoryObject = operationsList.find(object => object.type === category)
  const findIndexOfItem = getCategoryObject.operations
    .findIndex(operation => operation.key === operationName)
  if (findIndexOfItem >= 0)
  {
    return findIndexOfItem
  }
  return 0
}

export const copyToClipBoard = async clipboardValue => Clipboard.setString(clipboardValue)
