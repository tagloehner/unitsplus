import React from 'react'
import { View } from 'react-native'
import { Numpad, ScrollBar, Toast } from '../../components'
import { CombinedList } from './combinedList'
import { ConverterProps } from './interfaces'
import { converterStyles as styles } from './styles'


export const Converter: React.FC<ConverterProps> = ({ selectedCategory, themeStyles, index }) => {
  const { mainContainer } = styles
  return (
    <View style={{ ...mainContainer, ...themeStyles }}>
      <Toast />
      <CombinedList index={index} />
      <ScrollBar />
      <Numpad category={selectedCategory} />
    </View>
  )
}



