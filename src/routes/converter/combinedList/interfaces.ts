
import { i18nOperations, i18nPrefixes } from '../../../i18n/interfaces'
import { Operation } from '../../../models/interfaces'


export interface CombinedListProps extends CombinedListStateProps, ContainerInnerProps, ContainerOutterProps {
  listData: Operation[]
  category: string
}

export interface CombinedListStateProps {

}

export interface ContainerInnerProps {
  scrollToIndex: any
}

export interface ContainerOutterProps {
  index: number
}

export interface CombinedListItemProps extends CombinedListItemStateProps {
  listData: Operation[]
  category: string
}

export interface CombinedListItemStateProps {
  isDecimalsEnabled: boolean
  i18nOperations: i18nOperations
  i18nPrefixes: i18nPrefixes
  value: string
  result: string
  leftIndex: number
  rightIndex: number
  leftOperation: any
  rightOperation: any
}