import React from 'react'
import { View, FlatList, Dimensions } from 'react-native'
import CombinedListItem from './CombinedListItem'
import { operationsList } from '../../../models'
import { CombinedListProps } from './interfaces'


export const CombinedList: React.FC<CombinedListProps> = () => {
  const { width } = Dimensions.get('screen')
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={operationsList}
        ref={ref => { this.flatListRef = ref }}
        horizontal
        initialNumToRender={11}
        renderItem={({ item: { operations, type } }) => (
          <CombinedListItem
            listData={operations}
            category={type}
          />
        )}
        getItemLayout={(_, index) => ({
          length: width,
          offset: width * index,
          index,
        })}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_, index) => index.toString()}
        pagingEnabled
        scrollEnabled={false}
        bounces={false}
      />
    </View>
  )
}

