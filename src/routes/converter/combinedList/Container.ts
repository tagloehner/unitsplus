import { compose, withProps, lifecycle } from 'recompose'
import { CombinedList } from './CombinedList'
import { ContainerInnerProps, CombinedListStateProps, ContainerOutterProps, CombinedListProps } from './interfaces'


export const Container = compose<ContainerInnerProps, ContainerOutterProps>(
  withProps(() => ({
    scrollToIndex: (index: number) => this.flatListRef.scrollToIndex({ animated: false, index })
  })),
  lifecycle<CombinedListProps, CombinedListStateProps>({
    componentDidMount() {
      const { index, scrollToIndex } = this.props
      scrollToIndex(index)
    },
    componentDidUpdate() {
      const { index, scrollToIndex } = this.props
      scrollToIndex(index)
    }
  })
)(CombinedList)




