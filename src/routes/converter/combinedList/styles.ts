import { StyleSheet, Dimensions } from 'react-native'


export const combinedListItemStyles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
  },
  listContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    width: Dimensions.get('window').width,
  }
})
