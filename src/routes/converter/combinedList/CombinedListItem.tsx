import React from 'react'
import { View } from 'react-native'
import { LIST_LEFT, LIST_RIGHT } from '../../../constants'
import { Display, List, Gradient } from '../../../components'
import { CombinedListItemProps } from './interfaces'
import { combinedListItemStyles } from './styles'
import { connect } from 'react-redux'
import { makeGetCalculation, i18nOperations, i18nPrefixes, getIsDecimalsEnabled } from '../../../redux/selectors'
import { toLocaleString } from '../../../functions'


const CombinedListItem: React.FC<CombinedListItemProps> = ({
  i18nOperations,
  i18nPrefixes,
  listData,
  category,
  value,
  result,
  leftIndex,
  rightIndex,
  leftOperation,
  rightOperation,
  isDecimalsEnabled
}) => {
  const { mainContainer, listContainerStyle } = combinedListItemStyles
  return (
    <View style={{ ...mainContainer, flex: 1 }}>
      <View style={listContainerStyle}>
        <List
          data={listData}
          listIdentifier={LIST_LEFT}
          category={category}
          initialIndex={leftIndex}
        />
        <List
          data={listData}
          listIdentifier={LIST_RIGHT}
          category={category}
          initialIndex={rightIndex}
        />
      </View>
      <Gradient position="top" />
      <Display
        leftValue={value}
        rightValue={toLocaleString(isDecimalsEnabled ? Number(result).toFixed(2) : result)}
        leftTitle={i18nOperations[leftOperation]}
        leftSubtitle={i18nPrefixes[leftOperation]}
        rightTitle={i18nOperations[rightOperation]}
        rightSubtitle={i18nPrefixes[rightOperation]}
      />
      <Gradient position="bottom" />
    </View>
  )
}

const makeMapStateToProps = () => {
  const getCalculationResult = makeGetCalculation()
  const mapStateToProps = (state, props) => {
    const {
      value,
      result,
      leftIndex,
      rightIndex,
      leftOperation,
      rightOperation,
    } = getCalculationResult(state, props)
    return ({
      i18nOperations: i18nOperations(state),
      i18nPrefixes: i18nPrefixes(state),
      value: toLocaleString(value),
      result,
      isDecimalsEnabled: getIsDecimalsEnabled(state),
      leftIndex,
      rightIndex,
      leftOperation,
      rightOperation,
    })
  }
  return mapStateToProps
}

export default connect(makeMapStateToProps)(CombinedListItem)

