import { ThemeConverter } from '../../themes/interfaces'
import { Dispatch } from 'redux'


export interface ConverterProps extends ConverterStateProps, ConverterActionProps, ConverterHOCsProps { }

export interface ConverterStateProps {
  themeStyles: ThemeConverter
  selectedCategory: string
  index: number
}

export interface ConverterActionProps {
  setComponentsHeight: () => (dispatch: Dispatch) => void
  setThemeDark: (isThemeDark: boolean) => (dispatch: Dispatch) => any
}

export interface ConverterHOCsProps {
  isDarkMode: boolean
}