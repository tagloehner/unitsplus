import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { converterStyles } from '../../redux/selectors'
import { Converter } from './Converter'
import { setComponentsHeight, setThemeDark } from '../../redux/actionCreators'
import { ConverterProps, ConverterStateProps, ConverterActionProps } from './interfaces'
import { withDarkMode } from '../../HOCs'


const mapStateToProps = state => {
  const { scrollBar: { category, index } } = state
  return ({
    themeStyles: converterStyles(state),
    selectedCategory: category,
    index: index,
  })
}

const mapDispatchToProps = {
  setComponentsHeight,
  setThemeDark
}

export const Container = compose(
  withDarkMode,
  connect<ConverterStateProps, ConverterActionProps>(mapStateToProps, mapDispatchToProps),
  lifecycle<ConverterProps, ConverterStateProps>({
    componentDidUpdate() {
      const { isDarkMode, setThemeDark } = this.props
      setThemeDark(isDarkMode)
    },
    componentDidMount() {
      const { setComponentsHeight } = this.props
      setComponentsHeight()
    }
  }),
)(Converter)
