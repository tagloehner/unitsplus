import { StyleSheet } from 'react-native'


export const preferenceStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  sectionList: {
    flex: 1,
    borderRadius: 8,
  },
  touchableView: {
    flex: 1,
  },
  modalContainer: {
    borderRadius: 10,
    alignSelf: 'center',
    position: 'absolute',
    zIndex: 1,
    width: '85%',
  },
})