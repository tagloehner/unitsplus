import { connect } from 'react-redux'
import { preferencesStyles, i18nPreferences, i18nLanguage, getIsDecimalsEnabled } from '../../redux/selectors'
import { setLanguage, setDecimalsEnabled } from '../../redux/actionCreators'
import { Preferences } from './Preferences'
import { PreferencesStateProps, PreferencesActionProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: preferencesStyles(state),
  i18nPreferences: i18nPreferences(state),
  config: i18nLanguage(state),
  isDecimalsEnabled: getIsDecimalsEnabled(state),
})

const mapDispatchToProps = {
  setLanguage,
  setDecimalsEnabled,
}

export const Container = connect<PreferencesStateProps, PreferencesActionProps>(mapStateToProps, mapDispatchToProps)(Preferences)
