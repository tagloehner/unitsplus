import React from 'react'
import { View } from 'react-native'
import { separatorStyles as styles } from './styles'
import { SeparatorProps } from './interfaces'


export const Separator: React.FC<SeparatorProps> = () => {
  const { listSeparator } = styles
  return <View style={listSeparator} />
}
