import { StyleSheet } from 'react-native'


export const separatorStyles = StyleSheet.create({
  listSeparator: {
    height: 0.8,
  },
})