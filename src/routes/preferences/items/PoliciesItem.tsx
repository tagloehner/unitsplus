import React from 'react'
import { View, Text } from 'react-native'
import { policyItemstyles as styles } from './styles'
import { PolicyItemProps } from './interfaces'


export const PolicyItem: React.FC<PolicyItemProps> = ({ description, themeStyles }) => {
  const { text, container } = themeStyles
  const { mainContainer, textStyle } = styles

  return (
    <View style={{ ...mainContainer, ...container }}>
      <Text style={{ ...textStyle, ...text }}>{description}</Text>
    </View>
  )
}

