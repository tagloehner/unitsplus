import { ThemePreferencesThemeItem, ThemePreferencesLanguageItem, ThemePreferencesDecimalsItem, ThemePreferencesPolicyItem } from '../../../themes/interfaces'


export interface ThemeItemProps {
  title: string
  value: boolean
  themeStyles: ThemePreferencesThemeItem
  onValueChange: (bool: any) => (dispatch: any) => any
}

export interface LanguageItemProps {
  language: string
  selected: boolean
  onPress: () => (dispatch: any) => any
  themeStyles: ThemePreferencesLanguageItem
}

export interface DecimalsItemProps {
  title: string
  value: boolean
  themeStyles: ThemePreferencesDecimalsItem
  onValueChange: (bool: any) => (dispatch: any) => any
}

export interface PolicyItemProps {
  description: string
  themeStyles: ThemePreferencesPolicyItem
}