import React from 'react'
import { View, Text, Switch } from 'react-native'
import { themeItemStyles as styles } from './styles'
import { DecimalsItemProps } from './interfaces'


export const DecimalsItem: React.FC<DecimalsItemProps> = ({ title, onValueChange, value, themeStyles }) => {
  const { mainContainer, textRem } = styles
  const {
    backgroundColor,
    text,
    switch: { enabled, disabled },
  } = themeStyles
  return (
    <View style={{ ...mainContainer, backgroundColor }}>
      <Text style={{ ...text, ...textRem }}>{title}</Text>
      <Switch style={{ alignSelf: 'center', transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }] }} trackColor={{ false: disabled, true: enabled }}
        onValueChange={onValueChange}
        value={value}
      />
    </View>
  )
}