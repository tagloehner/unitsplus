import EStyleSheet from 'react-native-extended-stylesheet'


export const themeItemStyles = EStyleSheet.create({
  mainContainer: {
    height: '3rem',
    flexDirection: 'row',
    backgroundColor: '#2D2D2D',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '1.2rem / 2',
    paddingRight: '1.2rem / 3',
  },
  textRem: {
    fontSize: '1.2rem',
  },
})

export const languageItemstyles = EStyleSheet.create({
  mainContainer: {
    height: '3rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '1.2rem / 2',
  },
  textRem: {
    fontSize: '1.2rem',
  },
})

export const policyItemstyles = EStyleSheet.create({
  mainContainer: {
    textAlign: 'auto',
    padding: '1.2rem / 2',
  },
  textStyle: {
    fontSize: '1rem',
  },
})