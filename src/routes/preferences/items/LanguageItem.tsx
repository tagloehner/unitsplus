import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Radio } from 'native-base'
import { languageItemstyles as styles } from './styles'
import { LanguageItemProps } from './interfaces'


export const LanguageItem: React.FC<LanguageItemProps> = ({ language, selected, onPress, themeStyles }) => {
  const { mainContainer, textRem } = styles
  const {
    backgroundColor,
    radio,
    text: { color: { enabled, disabled } },
  } = themeStyles

  const textStyle = {
    ...textRem,
    color: selected ? enabled : disabled,
  }
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.4}
    >
      <View style={{ ...mainContainer, backgroundColor }}>
        <Text style={textStyle}>{language}</Text>
        <Radio
          selected={selected}
          selectedColor={radio.color}
        />
      </View>
    </TouchableOpacity>
  )
}


