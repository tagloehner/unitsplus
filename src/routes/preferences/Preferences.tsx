import React from 'react'
import { View, TouchableWithoutFeedback, SectionList } from 'react-native'
import { LanguageItem, DecimalsItem, PolicyItem } from './items'
import { Header } from './header'
import { Footer } from './footer'
import { preferencesModel } from './model'
import { Separator } from './separator'
import { switchCase } from '../../functions'
import { LANGUAGE, DECIMALS, POLICY } from '../../constants'
import { PreferencesProps } from './interfaces'
import { preferenceStyles as styles } from './styles'


export const Preferences: React.FC<PreferencesProps> = ({
  themeStyles,
  navigation,
  isDecimalsEnabled,
  i18nPreferences,
  config,
  setLanguage,
  setDecimalsEnabled
}) => {
  const { mainContainer, modalContainer, sectionList, touchableView } = styles
  const { container, languageItem, decimalsItem, header, policyItem } = themeStyles

  return (
    <View style={mainContainer}>
      <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
        <View style={{ ...touchableView }} />
      </TouchableWithoutFeedback>
      <View style={{ ...modalContainer }}>
        <SectionList
          bounces={false}
          style={{ ...container, ...sectionList }}
          sections={preferencesModel}
          renderSectionHeader={({ section: { title } }) => <Header title={i18nPreferences[title]} themeStyles={header} />}
          ListFooterComponent={() => <Footer />}
          renderItem={({ item, section: { title } }) => switchCase({
            [POLICY]: () => (
              <PolicyItem
                description={i18nPreferences[item.option]}
                themeStyles={policyItem}
              />
            ),
            [LANGUAGE]: () => (
              <LanguageItem
                language={i18nPreferences[item.option]}
                selected={config.language === item.option}
                onPress={() => setLanguage(item.option)}
                themeStyles={languageItem}
              />
            ),
            [DECIMALS]: () => (
              <DecimalsItem
                title={i18nPreferences[item.option]}
                onValueChange={() => setDecimalsEnabled(!isDecimalsEnabled)}
                value={isDecimalsEnabled}
                themeStyles={decimalsItem}
              />
            ),
          })(undefined)(title)}
          ItemSeparatorComponent={Separator}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item + index}
        />
      </View>
    </View>
  )
}
