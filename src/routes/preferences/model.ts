import { LANGUAGE, ENGLISH, PORTUGUESE, DECIMALS, DISPLAY_TWO_DECIMALS, POLICY, POLICY_DESCRIPTION } from '../../constants'
import { PreferenceModel } from './interfaces'


export const preferencesModel: PreferenceModel[] = [
  { title: LANGUAGE, data: [{ option: ENGLISH }, { option: PORTUGUESE }] },
  { title: DECIMALS, data: [{ option: DISPLAY_TWO_DECIMALS }] },
  { title: POLICY, data: [{ option: POLICY_DESCRIPTION }] },
]