import EStyleSheet from 'react-native-extended-stylesheet'


export const footerStyles = EStyleSheet.create({
  mainContainer: {
    backgroundColor: 'transparent',
    height: '1.6rem',
  },
})
