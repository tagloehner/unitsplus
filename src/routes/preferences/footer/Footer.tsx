import React from 'react'
import { View } from 'react-native'
import { FooterProps } from './interfaces'
import { footerStyles as styles } from './styles'


export const Footer: React.FC<FooterProps> = () => <View style={styles.mainContainer} />

