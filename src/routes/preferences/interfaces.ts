import { ThemePreferences } from '../../themes/interfaces'
import { i18nPrefences, i18nConfig } from '../../i18n/interfaces'
import { NavigationParams } from 'react-navigation'


export interface PreferencesProps extends PreferencesStateProps, PreferencesActionProps {
  navigation: NavigationParams
}

export interface PreferencesStateProps {
  themeStyles: ThemePreferences
  i18nPreferences: i18nPrefences
  config: i18nConfig
  isDecimalsEnabled: boolean
}

export interface PreferencesActionProps {
  setLanguage: (language: string) => (dispatch: any) => any
  setDecimalsEnabled: (isDecimalsEnabled: boolean) => (dispatch: any) => any
}

export type PreferenceModel = {
  title: string
  data: {
    option: string
  }[]
}