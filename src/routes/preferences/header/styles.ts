import EStyleSheet from 'react-native-extended-stylesheet'


export const headerStyles = EStyleSheet.create({
  mainContainer: {
    backgroundColor: 'transparent',
    height: '3rem',
    justifyContent: 'flex-end',
    padding: '0.6rem',
    paddingBottom: '0.4rem'
  },
  title: {
    fontSize: '0.7rem',
  }
})
