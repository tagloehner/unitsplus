import React from 'react'
import { View, Text } from 'react-native'
import { HeaderProps } from './interfaces'
import { headerStyles as styles } from './styles'


export const Header: React.FC<HeaderProps> = ({ title, themeStyles: { text: { color } } }) => (
  <View style={{ ...styles.mainContainer }}>
    <Text style={{ ...styles.title, color }}>{title.toUpperCase()}</Text>
  </View>
)
