import { ThemePreferencesHeader } from '../../../themes/interfaces'

export interface HeaderProps {
  title: string
  themeStyles: ThemePreferencesHeader
}