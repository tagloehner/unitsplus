import { Theme } from '../interfaces'

export const dark: Theme = {
  config: {
    theme: 'dark',
  },
  statusBar: {
    contentStyle: 'light-content',
  },
  safeAreaView: {
    backgroundColor: '#000000',
  },
  converter: {
    backgroundColor: '#000000',
  },
  display: {
    leftDisplay: {
      container: {
        backgroundColor: '#505050',
        flex: 1,
      },
      title: {
        fontSize: 16,
        color: '#FFFFFF',
      },
      value: {
        fontSize: 34,
        color: '#FFFFFF',
      },
      subtitle: {
        fontSize: 12,
        color: '#FFFFFF',
      },
    },
    rightDisplay: {
      container: {
        backgroundColor: '#FF9500',
        flex: 1,
      },
      title: {
        fontSize: 16,
        color: '#FFFFFF',
      },
      value: {
        fontSize: 34,
        color: '#FFFFFF',
      },
      subtitle: {
        fontSize: 12,
        color: '#FFFFFF',
      },
    },
  },
  list: {
    item: {
      borderColor: '#505050',
      title: {
        color: '#D4D4D2',
      }
    },
  },
  button: {
    backgroundColor: '#000000',
  },
  scrollBar: {
    backgroundColor: '#1C1C1C',
    item: {
      container: {
        backgroundColor: {
          enabled: '#1C1C1C',
          disabled: '#FF9500',
        },
        borderWidth: 1,
        borderColor: {
          enabled: '#1C1C1C',
          disabled: '#FF9500',
        },
      },
      text: {
        color: {
          enabled: '#D4D4D2',
          disabled: '#FFFFFF',
        },
      },
    },
  },
  numpad: {
    backgroundColor: '#000000',
    item: {
      number: {
        container: {
          backgroundColor: '#505050',
        },
        text: {
          color: '#FFFFFF',
        },
      },
      backspace: {
        container: {
          backgroundColor: '#FF9500',
        },
        text: {
          color: '#FFFFFF',
        },
        icon: {
          name: 'backspace',
          color: '#FFFFFF',
        },
      },
      allClear: {
        container: {
          backgroundColor: '#FF9500',
        },
        text: {
          color: '#FFFFFF',
        },
      },
      minusPlus: {
        container: {
          backgroundColor: '#FFFFFF',
        },
        text: {
          color: '#1C1C1C',
        },
      },
      copy: {
        container: {
          backgroundColor: '#FF9500',
        },
        text: {
          color: '#FFFFFF',
        },
        icon: {
          name: 'copy',
          color: '#FFFFFF',
        },
      },
      preferences: {
        container: {
          backgroundColor: '#FF9500',
        },
        icon: {
          name: 'ios-more',
          color: '#FFFFFF',
          type: 'Ionicons',
        },
        text: {
          color: '#FFFFFF',
        },
      },
      standard: {
        borderColor: '#000000',
        container: {
          backgroundColor: '#FFFFFF',
        },
        text: {
          color: '#1C1C1C',
        },
      },
    },
  },
  toast: {
    container: {
      backgroundColor: '#1C1C1C',
    },
    content: {
      color: '#FFFFFF',
    },
  },
  gradient: {
    topGradientColors: ['#000000'],
    bottomGradientColors: ['#000000'],
  },
  preferences: {
    policyItem: {
      text: {
        color: '#FFFFFF'
      },
      container: {
        backgroundColor: '#2D2D2D',
      }
    },
    container: {
      backgroundColor: '#1C1C1C',
    },
    header: {
      text: {
        color: '#D4D4D2'
      }
    },
    languageItem: {
      backgroundColor: '#2D2D2D',
      text: {
        color: {
          disabled: '#D4D4D2',
          enabled: '#FF9500',
        },
      },
      radio: {
        color: '#FF9500',
      },
    },
    themeItem: {
      backgroundColor: '#2D2D2D',
      text: {
        color: '#D4D4D2',
      },
      switch: {
        enabled: '#FF9500',
        disabled: '#D4D4D2',
      },
    },
    decimalsItem: {
      backgroundColor: '#2D2D2D',
      text: {
        color: '#D4D4D2',
      },
      switch: {
        enabled: '#FF9500',
        disabled: '#D4D4D2',
      },
    },
  },
}
