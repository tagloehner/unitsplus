export type Theme = {
  config: ThemeConfig,
  safeAreaView: ThemeSafeAreaView
  statusBar: ThemeStatusBar
  converter: ThemeConverter
  display: ThemeDisplay
  list: ThemeList
  button: ThemeButton
  scrollBar: ThemeScrollBar
  numpad: ThemeNumpad
  toast: ThemeToast
  gradient: ThemeGradient
  preferences: ThemePreferences
}
export type ThemeConfig = {
  theme: 'dark' | 'light'
}
export type ThemeSafeAreaView = {
  backgroundColor: string
}
export type ThemeStatusBar = {
  contentStyle: 'light-content' | 'dark-content'
}
export type ThemeConverter = {
  backgroundColor: string
}
export type ThemeDisplay = {
  leftDisplay: {
    container: {
      backgroundColor: string
      flex?: 1
    },
    title: {
      fontSize?: 16
      color: string
    },
    value: {
      fontSize?: 34
      color: string
    },
    subtitle: {
      fontSize?: 12
      color: string
    },
  },
  rightDisplay: {
    container: {
      backgroundColor: string
      flex?: 1
    },
    title: {
      fontSize?: 16
      color: string
    },
    value: {
      fontSize?: 34
      color: string
    },
    subtitle: {
      fontSize?: 12
      color: string
    },
  }
}
export type ThemeList = {
  item: ThemeListItem
}
export type ThemeListItem = {
  borderColor: string,
  title: {
    color: string,
  }
}
export type ThemeButton = {
  backgroundColor: string,
}
export type ThemeScrollBar = {
  backgroundColor: string,
  item: ThemeScrollBarItem
}
export type ThemeScrollBarItem = {
  container: {
    backgroundColor: {
      enabled: string,
      disabled: string,
    },
    borderWidth: number,
    borderColor: {
      enabled: string,
      disabled: string,
    },
  },
  text: {
    color: {
      enabled: string,
      disabled: string,
    },
  },
}

export type ThemeNumpadItem = {
  number: {
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
  },
  backspace: {
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
    icon: {
      name?: 'backspace',
      color: string,
    },
  },
  allClear: {
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
  },
  minusPlus: {
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
  },
  copy: {
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
    icon: {
      name?: 'copy',
      color: string,
    },
  },
  preferences: {
    container: {
      backgroundColor: string,
    },
    icon: {
      name?: 'ios-more',
      color: string,
      type?: 'Ionicons',
    },
    text: {
      color: string,
    },
  },
  standard: {
    borderColor: string
    container: {
      backgroundColor: string,
    },
    text: {
      color: string,
    },
  },
}
export type ThemeNumpad = {
  backgroundColor: string,
  item: ThemeNumpadItem
}
export type ThemeToast = {
  container: {
    backgroundColor: string,
  },
  content: {
    color: string,
  },
}
export type ThemeGradient = {
  topGradientColors?: string[],
  bottomGradientColors?: string[],
}
export type ThemePreferences = {
  container: {
    backgroundColor: string,
  },
  header: ThemePreferencesHeader
  languageItem: ThemePreferencesLanguageItem
  themeItem: ThemePreferencesThemeItem
  decimalsItem: ThemePreferencesDecimalsItem
  policyItem: ThemePreferencesPolicyItem
}
export type ThemePreferencesHeader = {
  text: {
    color: string
  }
}

export type ThemePreferencesLanguageItem = {
  backgroundColor: string,
  text: {
    color: {
      disabled: string,
      enabled: string,
    },
  },
  radio: {
    color: string,
  },
}

export type ThemePreferencesDecimalsItem = {
  backgroundColor: string,
  text: {
    color: string,
  },
  switch: {
    enabled: string,
    disabled: string,
  },
}

export type ThemePreferencesThemeItem = {
  backgroundColor: string,
  text: {
    color: string,
  },
  switch: {
    enabled: string,
    disabled: string,
  },
}

export type ThemePreferencesPolicyItem = {
  text: {
    color: string,
  },
  container: {
    backgroundColor: string
  }
}