import * as CONSTANTS from '../constants'
import { OperationsList } from './interfaces'


export const operationsList: OperationsList[] = [
  {
    type: CONSTANTS.AREA,
    operations: [
      { key: CONSTANTS.HECTARE },
      { key: CONSTANTS.ARE },
      { key: CONSTANTS.SQUARE_KILOMETER },
      { key: CONSTANTS.SQUARE_METER },
      { key: CONSTANTS.SQUARE_DECIMETER },
      { key: CONSTANTS.SQUARE_CENTIMETER },
      { key: CONSTANTS.SQUARE_MILLIMETER },
      { key: CONSTANTS.SQUARE_MILE },
      { key: CONSTANTS.ACRE },
      { key: CONSTANTS.SQUARE_YARD },
      { key: CONSTANTS.SQUARE_FOOT },
      { key: CONSTANTS.SQUARE_INCH }
    ]
  },
  {
    type: CONSTANTS.DATA,
    operations: [
      { key: CONSTANTS.BIT },
      { key: CONSTANTS.BYTE },
      { key: CONSTANTS.KILOBYTE },
      { key: CONSTANTS.MEGABYTE },
      { key: CONSTANTS.GIGABYTE },
      { key: CONSTANTS.TERABYTE }
    ]
  },
  {
    type: CONSTANTS.FUEL,
    operations: [
      { key: CONSTANTS.MILES_GALLON_US },
      { key: CONSTANTS.MILES_GALLON_UK },
      { key: CONSTANTS.LITERS_PER_ONE_HUNDRED_KM },
      { key: CONSTANTS.LITERS_PER_KM },
      { key: CONSTANTS.KILOMETERS_PER_LITER }
    ]
  },
  {
    type: CONSTANTS.LENGTH,
    operations: [
      { key: CONSTANTS.KILOMETER },
      { key: CONSTANTS.METER },
      { key: CONSTANTS.DECIMETER },
      { key: CONSTANTS.CENTIMER },
      { key: CONSTANTS.MILLIMETER },
      { key: CONSTANTS.MILE },
      { key: CONSTANTS.YARD },
      { key: CONSTANTS.FEET },
      { key: CONSTANTS.INCH },
      { key: CONSTANTS.NAUTICAL_MILE },
      { key: CONSTANTS.LEAGUE },
      { key: CONSTANTS.FURLONG },
      { key: CONSTANTS.ROD },
      { key: CONSTANTS.CHAIN }
    ]
  },
  {
    type: CONSTANTS.POWER,
    operations: [
      { key: CONSTANTS.BTU_H },
      { key: CONSTANTS.HORSE_POWER },
      { key: CONSTANTS.KILOWATT },
      { key: CONSTANTS.WATT },
      { key: CONSTANTS.TON_OF_REFRIGERATION }
    ]
  },
  {
    type: CONSTANTS.PRESSURE,
    operations: [
      { key: CONSTANTS.ATMOSPHERE },
      { key: CONSTANTS.BAR },
      { key: CONSTANTS.MILLIBAR },
      { key: CONSTANTS.INCH_MERCURY },
      { key: CONSTANTS.MILLIMETER_MERCURY },
      { key: CONSTANTS.MEGA_PASCAL },
      { key: CONSTANTS.KILO_PASCAL },
      { key: CONSTANTS.PASCAL_PA },
      { key: CONSTANTS.POUND_PSF },
      { key: CONSTANTS.POUND_PSI }
    ]
  },
  {
    type: CONSTANTS.SPEED,
    operations: [
      { key: CONSTANTS.KILOMETER_PER_HOUR },
      { key: CONSTANTS.METER_PER_SECOND },
      { key: CONSTANTS.MILE_PER_HOUR },
      { key: CONSTANTS.FEET_PER_MINUTE },
      { key: CONSTANTS.FEET_PER_SECOND },
      { key: CONSTANTS.KNOT },
      { key: CONSTANTS.SPEED_OF_LIGHT },
      { key: CONSTANTS.SPEED_OF_SOUND }
    ]
  },
  {
    type: CONSTANTS.TEMPERATURE,
    operations: [
      { key: CONSTANTS.CELSIUS },
      { key: CONSTANTS.FAHRENHEIT },
      { key: CONSTANTS.KELVIN },
      { key: CONSTANTS.RANKINE }
    ]
  },
  {
    type: CONSTANTS.TIME,
    operations: [
      { key: CONSTANTS.MILLENNIUM },
      { key: CONSTANTS.CENTURY },
      { key: CONSTANTS.DECADE },
      { key: CONSTANTS.CALENDAR_YEAR },
      { key: CONSTANTS.MONTH },
      { key: CONSTANTS.WEEK },
      { key: CONSTANTS.DAY },
      { key: CONSTANTS.HOUR },
      { key: CONSTANTS.MINUTE },
      { key: CONSTANTS.SECOND },
      { key: CONSTANTS.MILLISECOND }
    ]
  },
  {
    type: CONSTANTS.VOLUME,
    operations: [
      { key: CONSTANTS.CUBIC_METER },
      { key: CONSTANTS.LITER },
      { key: CONSTANTS.DECILITER },
      { key: CONSTANTS.CENTILITER },
      { key: CONSTANTS.MILLILITER },
      { key: CONSTANTS.CUBIC_MILLIMETER },
      { key: CONSTANTS.TABLESPOON },
      { key: CONSTANTS.TEASPOON },
      { key: CONSTANTS.CUBIC_YARD },
      { key: CONSTANTS.CUBIC_FOOT },
      { key: CONSTANTS.CUBIC_INCH },
      { key: CONSTANTS.US_BUSHEL },
      { key: CONSTANTS.US_BARREL },
      { key: CONSTANTS.US_LIQUID_GALLON },
      { key: CONSTANTS.US_DRY_GALLON },
      { key: CONSTANTS.US_FLUID_OUNCE },
      { key: CONSTANTS.US_PINT },
      { key: CONSTANTS.US_QUART },
      { key: CONSTANTS.US_TABLESPOON },
      { key: CONSTANTS.US_TEASPOON },
      { key: CONSTANTS.US_CUP },
      { key: CONSTANTS.UK_BARREL },
      { key: CONSTANTS.UK_GALLON },
      { key: CONSTANTS.UK_FLUID_OUNCE },
      { key: CONSTANTS.UK_PINT },
      { key: CONSTANTS.UK_QUART }
    ]
  },
  {
    type: CONSTANTS.WEIGHT,
    operations: [
      { key: CONSTANTS.KILOGRAM },
      { key: CONSTANTS.GRAM },
      { key: CONSTANTS.MILLIGRAM },
      { key: CONSTANTS.POUND },
      { key: CONSTANTS.OUNCE },
      { key: CONSTANTS.TON_METRIC },
      { key: CONSTANTS.TON_LONG },
      { key: CONSTANTS.TON_SHORT },
      { key: CONSTANTS.STONE }
    ]
  }
]
