import * as CONSTANTS from '../constants'


export type OperationsList = {
  type: string,
  operations: Operation[]
}

export type Operation = {
  key: string
}