import { ViewStyle } from 'react-native'

export interface WithSafeAreaProps extends ViewStyle {
}
