import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import { WithSafeAreaProps } from './interfaces'

export const withSafeArea = <P extends object>(
  Component: React.ComponentType<P>
): React.FC<P & WithSafeAreaProps> => ({
  backgroundColor,
  ...props
}: WithSafeAreaProps) => (
      <SafeAreaView style={{
        ...StyleSheet.absoluteFillObject,
        backgroundColor
      }}>
        <Component {...(props as P)} />
      </SafeAreaView>
    )


