export interface WithdarkModeProps {
  isDarkMode: boolean
}