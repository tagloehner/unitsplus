import React, { Fragment } from 'react'
import { WithdarkModeProps } from './interfaces'
import { useDarkMode } from 'react-native-dark-mode'


export const withDarkMode = <P extends object>(
  Component: React.ComponentType<P>
): React.FC<P & WithdarkModeProps> => ({
  ...props
}: WithdarkModeProps) => {
    const isDarkMode = useDarkMode()
    return (
      <Fragment>
        <Component {...(props as P)} isDarkMode={isDarkMode} />
      </Fragment>
    )
  }


