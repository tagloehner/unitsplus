import { createAppContainer } from 'react-navigation'
import { ConverterStack } from './stacks'


export const AppNavigator = createAppContainer(ConverterStack)
