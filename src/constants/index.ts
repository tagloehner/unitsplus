export const CONVERTER = 'Converter';
export const CATEGORIES = 'Categories';
export const PREFERENCES = 'Preferences';
export const CONVERTERSTACK = 'ConverterStack';
export const MAINSTACK = 'MainStack';

export const LIST_LEFT = 'listLeft';
export const LIST_RIGHT = 'listRight';

export const HECTARE = 'area/hectare';
export const ARE = 'area/are';
export const SQUARE_KILOMETER = 'area/squareKilometer';
export const SQUARE_METER = 'area/squareMeter';
export const SQUARE_DECIMETER = 'area/squareDecimeter';
export const SQUARE_CENTIMETER = 'area/squareCentimeter';
export const SQUARE_MILLIMETER = 'area/squareMillimeter';
export const SQUARE_MILE = 'area/squareMile';
export const ACRE = 'area/acre';
export const SQUARE_YARD = 'area/squareYard';
export const SQUARE_FOOT = 'area/squareFoot';
export const SQUARE_INCH = 'area/squareInch';

export const AREA = 'area';
export const CURRENCY = 'currency';
export const DATA = 'data';
export const FUEL = 'fuel';
export const LENGTH = 'length';
export const POWER = 'power';
export const PRESSURE = 'pressure';
export const SPEED = 'speed';
export const TEMPERATURE = 'temperature';
export const TIME = 'time';
export const VOLUME = 'volume';
export const WEIGHT = 'weight';

export const KILOMETER = 'length/kilometer';
export const METER = 'length/meter';
export const DECIMETER = 'length/decimeter';
export const CENTIMER = 'length/centimeter';
export const MILLIMETER = 'length/millimeter';
export const MILE = 'length/mile';
export const YARD = 'length/yard';
export const FEET = 'length/feet';
export const INCH = 'length/inch';
export const NAUTICAL_MILE = 'nauticalMile';
export const LEAGUE = 'league';
export const FURLONG = 'furlong';
export const ROD = 'rod';
export const CHAIN = 'chain';

export const KILOGRAM = 'weight/kilogram';
export const GRAM = 'weight/gram';
export const MILLIGRAM = 'weight/milligram';
export const POUND = 'weight/pound';
export const OUNCE = 'weight/ounce';
export const TON_METRIC = 'weight/tonMetric';
export const TON_LONG = 'weight/tonLong';
export const TON_SHORT = 'weight/tonShort';
export const STONE = 'weight/stone';

export const CELSIUS = 'temperature/celsius';
export const FAHRENHEIT = 'temperature/fahrenheit';
export const KELVIN = 'temperature/kelvin';
export const RANKINE = 'temperature/rankine';

export const BIT = 'data/Bit';
export const BYTE = 'data/Byte';
export const KILOBYTE = 'data/Kilobyte';
export const MEGABYTE = 'data/Megabyte';
export const GIGABYTE = 'data/Gigabyte';
export const TERABYTE = 'data/Terabyte';

export const MILES_GALLON_US = 'fuel/milesPerGallonUs';
export const MILES_GALLON_UK = 'fuel/milesPerGallonUk';
export const LITERS_PER_ONE_HUNDRED_KM = 'fuel/litersPerOneHundredKm';
export const LITERS_PER_KM = 'fuel/literPerKm';
export const KILOMETERS_PER_LITER = 'fuel/kilometersPerLiter';

export const BTU_H = 'power/btuh';
export const HORSE_POWER = 'power/horsePower';
export const KILOWATT = 'power/kilowatt';
export const WATT = 'power/watt';
export const TON_OF_REFRIGERATION = 'power/tonOfRefrigeration';

export const MILLENNIUM = 'time/millennium';
export const CENTURY = 'time/century';
export const DECADE = 'time/decade';
export const CALENDAR_YEAR = 'time/calendarYear';
export const MONTH = 'time/month';
export const WEEK = 'time/week';
export const DAY = 'time/day';
export const HOUR = 'time/hour';
export const MINUTE = 'time/minute';
export const SECOND = 'time/second';
export const MILLISECOND = 'time/millisecond';

export const ATMOSPHERE = 'pressure/atmosphere';
export const BAR = 'pressure/bar';
export const MILLIBAR = 'pressure/millibar';
export const INCH_MERCURY = 'pressure/inchMercury';
export const MILLIMETER_MERCURY = 'pressure/millimeterMercury';
export const MEGA_PASCAL = 'pressure/megaPascal';
export const KILO_PASCAL = 'pressure/kiloPascal';
export const PASCAL_PA = 'pressure/pascalPerSquareMeter';
export const POUND_PSF = 'pressure/poundPerSquareFoot';
export const POUND_PSI = 'pressure/poundPerSquareInch';

export const KILOMETER_PER_HOUR = 'speed/kilometerPerHour';
export const METER_PER_SECOND = 'speed/meterPerSecond';
export const MILE_PER_HOUR = 'speed/milePerHour';
export const FEET_PER_MINUTE = 'speed/feetPerMinute';
export const FEET_PER_SECOND = 'speed/feetPerSecond';
export const KNOT = 'speed/knot';
export const SPEED_OF_LIGHT = 'speed/speedOfLight';
export const SPEED_OF_SOUND = 'speed/speedOfSound';

export const CUBIC_METER = 'volume/cubicMeter';
export const LITER = 'volume/liter';
export const DECILITER = 'volume/deciliter';
export const CENTILITER = 'volume/centiliter';
export const MILLILITER = 'volume/milliliter';
export const CUBIC_MILLIMETER = 'volume/cubicMilliliter';
export const TABLESPOON = 'volume/tablespoon';
export const TEASPOON = 'volume/teaspoon';
export const CUBIC_YARD = 'volume/cubicYard';
export const CUBIC_FOOT = 'volume/cubicFoot';
export const CUBIC_INCH = 'volume/cubicInch';
export const US_BUSHEL = 'volume/usBushel';
export const US_BARREL = 'volume/usBarrel';
export const US_LIQUID_GALLON = 'volume/usLiquidGalon';
export const US_DRY_GALLON = 'volume/usDryGalon';
export const US_FLUID_OUNCE = 'volume/usFluidOunce';
export const US_PINT = 'volume/usPint';
export const US_QUART = 'volume/usQuart';
export const US_TABLESPOON = 'volume/usTablespoon';
export const US_TEASPOON = 'volume/usTeaspoon';
export const US_CUP = 'volume/usCup';
export const UK_BARREL = 'volume/ukBarrel';
export const UK_GALLON = 'volume/ukGallon';
export const UK_FLUID_OUNCE = 'volume/ukFluidOunce';
export const UK_PINT = 'volume/ukPint';
export const UK_QUART = 'volume/ukQuart';

export const EMAIL_ADRESS = 'steven.tagloehner@outlook.com';

export const COPY = 'COPY';
export const MESSAGE_COPY = 'Copied to clipboard!';
export const AC = 'AC';

export const LANGUAGE = 'Language';
export const ENGLISH = 'English';
export const PORTUGUESE = 'Portuguese';
export const THEME = 'Theme';
export const DARK_MODE = 'Dark Mode';
export const CONTACT = 'Contact';
export const DECIMALS = 'Decimals'
export const DISPLAY_TWO_DECIMALS = 'Display Two Decimals'
export const POLICY = 'Policy'
export const POLICY_DESCRIPTION = 'Policy Description'