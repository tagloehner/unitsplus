import * as CONST from '../constants'


export type i18nConfig = {
  language: string
}
export type i18nCategories = {
  [CONST.AREA]: string
  [CONST.DATA]: string
  [CONST.FUEL]: string
  [CONST.LENGTH]: string
  [CONST.POWER]: string
  [CONST.PRESSURE]: string
  [CONST.SPEED]: string
  [CONST.TEMPERATURE]: string
  [CONST.TIME]: string
  [CONST.VOLUME]: string
  [CONST.WEIGHT]: string
}

export type i18nOperations = {
  [CONST.HECTARE]: string
  [CONST.ARE]: string
  [CONST.SQUARE_KILOMETER]: string
  [CONST.SQUARE_METER]: string
  [CONST.SQUARE_DECIMETER]: string
  [CONST.SQUARE_CENTIMETER]: string
  [CONST.SQUARE_MILLIMETER]: string
  [CONST.SQUARE_MILE]: string
  [CONST.ACRE]: string
  [CONST.SQUARE_YARD]: string
  [CONST.SQUARE_FOOT]: string
  [CONST.SQUARE_INCH]: string
  [CONST.BIT]: string
  [CONST.BYTE]: string
  [CONST.KILOBYTE]: string
  [CONST.MEGABYTE]: string
  [CONST.GIGABYTE]: string
  [CONST.TERABYTE]: string
  [CONST.MILES_GALLON_US]: string
  [CONST.MILES_GALLON_UK]: string
  [CONST.LITERS_PER_ONE_HUNDRED_KM]: string
  [CONST.LITERS_PER_KM]: string
  [CONST.KILOMETERS_PER_LITER]: string
  [CONST.KILOMETER]: string
  [CONST.METER]: string
  [CONST.DECIMETER]: string
  [CONST.CENTIMER]: string
  [CONST.MILLIMETER]: string
  [CONST.MILE]: string
  [CONST.YARD]: string
  [CONST.FEET]: string
  [CONST.INCH]: string
  [CONST.NAUTICAL_MILE]: string
  [CONST.LEAGUE]: string
  [CONST.FURLONG]: string
  [CONST.ROD]: string
  [CONST.CHAIN]: string
  [CONST.BTU_H]: string
  [CONST.HORSE_POWER]: string
  [CONST.KILOWATT]: string
  [CONST.WATT]: string
  [CONST.TON_OF_REFRIGERATION]: string
  [CONST.ATMOSPHERE]: string
  [CONST.BAR]: string
  [CONST.MILLIBAR]: string
  [CONST.INCH_MERCURY]: string
  [CONST.MILLIMETER_MERCURY]: string
  [CONST.MEGA_PASCAL]: string
  [CONST.KILO_PASCAL]: string
  [CONST.PASCAL_PA]: string
  [CONST.POUND_PSF]: string
  [CONST.POUND_PSI]: string
  [CONST.KILOMETER_PER_HOUR]: string
  [CONST.METER_PER_SECOND]: string
  [CONST.MILE_PER_HOUR]: string
  [CONST.FEET_PER_MINUTE]: string
  [CONST.FEET_PER_SECOND]: string
  [CONST.KNOT]: string
  [CONST.SPEED_OF_LIGHT]: string
  [CONST.SPEED_OF_SOUND]: string
  [CONST.CELSIUS]: string
  [CONST.FAHRENHEIT]: string
  [CONST.KELVIN]: string
  [CONST.RANKINE]: string
  [CONST.MILLENNIUM]: string
  [CONST.CENTURY]: string
  [CONST.DECADE]: string
  [CONST.CALENDAR_YEAR]: string
  [CONST.MONTH]: string
  [CONST.WEEK]: string
  [CONST.DAY]: string
  [CONST.HOUR]: string
  [CONST.MINUTE]: string
  [CONST.SECOND]: string
  [CONST.MILLISECOND]: string
  [CONST.CUBIC_METER]: string
  [CONST.LITER]: string
  [CONST.DECILITER]: string
  [CONST.CENTILITER]: string
  [CONST.MILLILITER]: string
  [CONST.CUBIC_MILLIMETER]: string
  [CONST.TABLESPOON]: string
  [CONST.TEASPOON]: string
  [CONST.CUBIC_YARD]: string
  [CONST.CUBIC_FOOT]: string
  [CONST.CUBIC_INCH]: string
  [CONST.US_BUSHEL]: string
  [CONST.US_BARREL]: string
  [CONST.US_LIQUID_GALLON]: string
  [CONST.US_DRY_GALLON]: string
  [CONST.US_FLUID_OUNCE]: string
  [CONST.US_PINT]: string
  [CONST.US_QUART]: string
  [CONST.US_TABLESPOON]: string
  [CONST.US_TEASPOON]: string
  [CONST.US_CUP]: string
  [CONST.UK_BARREL]: string
  [CONST.UK_GALLON]: string
  [CONST.UK_FLUID_OUNCE]: string
  [CONST.UK_PINT]: string
  [CONST.UK_QUART]: string
  [CONST.KILOGRAM]: string
  [CONST.GRAM]: string
  [CONST.MILLIGRAM]: string
  [CONST.POUND]: string
  [CONST.OUNCE]: string
  [CONST.TON_METRIC]: string
  [CONST.TON_LONG]: string
  [CONST.TON_SHORT]: string
  [CONST.STONE]: string
}
export type i18nPrefixes = {
  [CONST.HECTARE]: string
  [CONST.ARE]: string
  [CONST.SQUARE_KILOMETER]: string
  [CONST.SQUARE_METER]: string
  [CONST.SQUARE_DECIMETER]: string
  [CONST.SQUARE_CENTIMETER]: string
  [CONST.SQUARE_MILLIMETER]: string
  [CONST.SQUARE_MILE]: string
  [CONST.ACRE]: string
  [CONST.SQUARE_YARD]: string
  [CONST.SQUARE_FOOT]: string
  [CONST.SQUARE_INCH]: string
  [CONST.BIT]: string
  [CONST.BYTE]: string
  [CONST.KILOBYTE]: string
  [CONST.MEGABYTE]: string
  [CONST.GIGABYTE]: string
  [CONST.TERABYTE]: string
  [CONST.MILES_GALLON_US]: string
  [CONST.MILES_GALLON_UK]: string
  [CONST.LITERS_PER_ONE_HUNDRED_KM]: string
  [CONST.LITERS_PER_KM]: string
  [CONST.KILOMETERS_PER_LITER]: string
  [CONST.KILOMETER]: string
  [CONST.METER]: string
  [CONST.DECIMETER]: string
  [CONST.CENTIMER]: string
  [CONST.MILLIMETER]: string
  [CONST.MILE]: string
  [CONST.YARD]: string
  [CONST.FEET]: string
  [CONST.INCH]: string
  [CONST.NAUTICAL_MILE]: string
  [CONST.LEAGUE]: string
  [CONST.FURLONG]: string
  [CONST.ROD]: string
  [CONST.CHAIN]: string
  [CONST.BTU_H]: string
  [CONST.HORSE_POWER]: string
  [CONST.KILOWATT]: string
  [CONST.WATT]: string
  [CONST.TON_OF_REFRIGERATION]: string
  [CONST.ATMOSPHERE]: string
  [CONST.BAR]: string
  [CONST.MILLIBAR]: string
  [CONST.INCH_MERCURY]: string
  [CONST.MILLIMETER_MERCURY]: string
  [CONST.MEGA_PASCAL]: string
  [CONST.KILO_PASCAL]: string
  [CONST.PASCAL_PA]: string
  [CONST.POUND_PSF]: string
  [CONST.POUND_PSI]: string
  [CONST.KILOMETER_PER_HOUR]: string
  [CONST.METER_PER_SECOND]: string
  [CONST.MILE_PER_HOUR]: string
  [CONST.FEET_PER_MINUTE]: string
  [CONST.FEET_PER_SECOND]: string
  [CONST.KNOT]: string
  [CONST.SPEED_OF_LIGHT]: string
  [CONST.SPEED_OF_SOUND]: string
  [CONST.CELSIUS]: string
  [CONST.FAHRENHEIT]: string
  [CONST.KELVIN]: string
  [CONST.RANKINE]: string
  [CONST.MILLENNIUM]: string
  [CONST.CENTURY]: string
  [CONST.DECADE]: string
  [CONST.CALENDAR_YEAR]: string
  [CONST.MONTH]: string
  [CONST.WEEK]: string
  [CONST.DAY]: string
  [CONST.HOUR]: string
  [CONST.MINUTE]: string
  [CONST.SECOND]: string
  [CONST.MILLISECOND]: string
  [CONST.CUBIC_METER]: string
  [CONST.LITER]: string
  [CONST.DECILITER]: string
  [CONST.CENTILITER]: string
  [CONST.MILLILITER]: string
  [CONST.CUBIC_MILLIMETER]: string
  [CONST.TABLESPOON]: string
  [CONST.TEASPOON]: string
  [CONST.CUBIC_YARD]: string
  [CONST.CUBIC_FOOT]: string
  [CONST.CUBIC_INCH]: string
  [CONST.US_BUSHEL]: string
  [CONST.US_BARREL]: string
  [CONST.US_LIQUID_GALLON]: string
  [CONST.US_DRY_GALLON]: string
  [CONST.US_FLUID_OUNCE]: string
  [CONST.US_PINT]: string
  [CONST.US_QUART]: string
  [CONST.US_TABLESPOON]: string
  [CONST.US_TEASPOON]: string
  [CONST.US_CUP]: string
  [CONST.UK_BARREL]: string
  [CONST.UK_GALLON]: string
  [CONST.UK_FLUID_OUNCE]: string
  [CONST.UK_PINT]: string
  [CONST.UK_QUART]: string
  [CONST.KILOGRAM]: string
  [CONST.GRAM]: string
  [CONST.MILLIGRAM]: string
  [CONST.POUND]: string
  [CONST.OUNCE]: string
  [CONST.TON_METRIC]: string
  [CONST.TON_LONG]: string
  [CONST.TON_SHORT]: string
  [CONST.STONE]: string
}
export type i18nNumpad = {
  [CONST.COPY]: string
  [CONST.MESSAGE_COPY]: string
  [CONST.AC]: string
}
export type i18nPrefences = {
  [CONST.LANGUAGE]: string
  [CONST.ENGLISH]: string
  [CONST.PORTUGUESE]: string
  [CONST.THEME]: string
  [CONST.DARK_MODE]: string
  [CONST.CONTACT]: string
  [CONST.DECIMALS]: string
  [CONST.DISPLAY_TWO_DECIMALS]: string
  [CONST.POLICY_DESCRIPTION]: string
  [CONST.POLICY]: string,
}

export type i18n = {
  config: i18nConfig
  categories: i18nCategories
  operations: i18nOperations
  prefixes: i18nPrefixes
  numpad: i18nNumpad
  preferences: i18nPrefences
}