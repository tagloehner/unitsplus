import { connect } from 'react-redux'
import { compose, withState, withProps, lifecycle } from 'recompose'
import { listStyles, i18nOperations, listItemHeight, i18nPrefixes } from '../../redux/selectors'
import { onSelectedItemUpdate } from '../../redux/actionCreators'
import { List } from './List'
import { ListStateProps, ListActionProps, ListProps, ContainerInnerProps, ContainerOutterProps } from './interfaces'
import SplashScreen from 'react-native-splash-screen'


const mapStateToProps = state => ({
  themeStyles: listStyles(state),
  i18nOperations: i18nOperations(state),
  i18nPrefixes: i18nPrefixes(state),
  listItemHeight: listItemHeight(state),
})

export const Container = compose<ContainerInnerProps, ContainerOutterProps>(
  connect<ListStateProps, ListActionProps>(mapStateToProps, { onSelectedItemUpdate }),
  withState('currentIndex', 'setCurrentIndex', 0),
  withState('currentItemHeight', 'setCurrentItemHeight', 0),
  withProps(({ data, listItemHeight, currentIndex, setCurrentIndex, setCurrentItemHeight }) => ({
    initialIndex: 0,
    scrollToIndex: (index, time = 0) => {
      setTimeout(() => {
        this.flatListRef.scrollToIndex({ animated: false, index })
      }, time)
    },
    getListItemHeight: (height: number) => setCurrentItemHeight(height),
    handleScroll: event => {
      const index = event.nativeEvent.contentOffset.y / listItemHeight
      const roundedIndex = Math.round(index)
      const validatedIndex = roundedIndex <= 0
        ? 0
        : roundedIndex > data.length - 1
          ? data.length - 1
          : roundedIndex
      if (currentIndex !== validatedIndex)
      {
        setCurrentIndex(validatedIndex)
      }
    }
  })),
  lifecycle<ListProps, ListStateProps>({
    componentDidMount() {
      const { initialIndex, scrollToIndex, currentItemHeight, listItemHeight } = this.props
      scrollToIndex(initialIndex)
      if (currentItemHeight === listItemHeight && currentItemHeight > 0)
      {
        SplashScreen.hide()
      }
    },
    componentDidUpdate() {
      const { currentItemHeight, listItemHeight } = this.props
      if (currentItemHeight === listItemHeight && currentItemHeight > 0)
      {
        SplashScreen.hide()
      }
    }
  })
)(List)
