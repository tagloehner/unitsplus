
import { ThemeList, ThemeListItem } from '../../themes/interfaces'
import { i18nOperations, i18nPrefixes } from '../../i18n/interfaces'
import { Operation } from '../../models/interfaces'

export interface ListProps extends ListStateProps, ListActionProps, ContainerOutterProps, ContainerInnerProps { }

export interface ListStateProps {
  themeStyles: ThemeList
  i18nOperations: i18nOperations
  i18nPrefixes: i18nPrefixes
  listItemHeight: number
}
export interface ListActionProps {
  onSelectedItemUpdate: (payload: any) => (dispatch: any) => any
}

export interface ContainerInnerProps {
  scrollToIndex: (index: number, time?: number) => void
  handleScroll: (event: any) => void
  currentIndex: number
  currentItemHeight: number
  getListItemHeight: (number: any) => number
}
export interface ContainerOutterProps {
  data: Operation[]
  listIdentifier: string
  category: string
  initialIndex: number
}

export interface ListItemProps {
  themeStyles: ThemeListItem
  title: string
  itemHeight: number
  getItemHeight: (height: number) => number
}