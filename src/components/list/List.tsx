import React from 'react'
import { FlatList, Dimensions } from 'react-native'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import { ListItem } from './ListItem'
import { ListProps } from './interfaces'
import { listStyles as styles } from './styles'


export const List: React.FC<ListProps> = ({
  data,
  listIdentifier,
  onSelectedItemUpdate,
  category,
  themeStyles,
  i18nOperations,
  listItemHeight,
  currentIndex,
  handleScroll,
  getListItemHeight,
}) => {

  const { height } = Dimensions.get('screen')
  const numberOfRows = height >= 812 ? 7 : 5
  const threshold = ((listItemHeight * numberOfRows) / 2) - (listItemHeight / 2)

  const contentContainerStyle = {
    flexGrow: 1,
    paddingTop: threshold,
    paddingBottom: threshold,
  }
  const { mainContainerStyle } = styles

  return (
    <FlatList
      style={mainContainerStyle}
      data={data}
      ref={ref => { this.flatListRef = ref }}
      extraData={{ i18nOperations, currentIndex, listItemHeight }}
      initialScrollIndex={0}
      contentContainerStyle={contentContainerStyle}
      decelerationRate="fast"
      onScroll={(event) => handleScroll(event)}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      keyExtractor={(_, index) => index.toString()}
      snapToInterval={listItemHeight}
      renderItem={({ item: { key } }) => (
        <ListItem
          title={i18nOperations[key]}
          themeStyles={themeStyles.item}
          itemHeight={listItemHeight}
          getItemHeight={(height: number) => getListItemHeight(height)}
        />
      )}
      getItemLayout={(_, index) => ({
        length: listItemHeight,
        offset: listItemHeight * index,
        index,
      })}
      onMomentumScrollEnd={() => {
        ReactNativeHapticFeedback.trigger('selection')
        onSelectedItemUpdate({
          listIdentifier,
          operation: data[currentIndex].key,
          category,
          index: currentIndex
        })
      }}
    />
  )
}

