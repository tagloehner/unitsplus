import React from 'react'
import { View, Text } from 'react-native'
import { listItemStyles as styles } from './styles'
import { ListItemProps } from './interfaces'


export const ListItem: React.FC<ListItemProps> = ({ themeStyles, title, itemHeight, getItemHeight }) => {
  const { mainContainer, titleTextLabel } = styles
  return (
    <View onLayout={event => getItemHeight(event.nativeEvent.layout.height)} style={{ ...mainContainer, height: itemHeight, borderColor: themeStyles.borderColor }}>
      <Text style={{ ...titleTextLabel, ...themeStyles.title }} numberOfLines={1}>
        {title}
      </Text>
    </View>
  )
}
