
import EStyleSheet from 'react-native-extended-stylesheet'


export const listStyles = EStyleSheet.create({
  mainContainerStyle: {
    marginLeft: '1rem / 2',
    marginRight: '1rem / 2',
  },
})

export const listItemStyles = EStyleSheet.create({
  mainContainer: {
    borderTopWidth: '1rem / 32',
  },
  titleTextLabel: {
    fontSize: '0.8rem',
    position: 'absolute',
    left: '1rem / 4',
    top: '1rem / 2',
    right: '1rem / 4',
  }
})
