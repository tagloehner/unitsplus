import EStyleSheet from 'react-native-extended-stylesheet'


export const styles = EStyleSheet.create({
  animatedView: {
    position: 'absolute',
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    height: 140,
    width: 140,
    borderRadius: '1.4rem',
  },
  contentText: {
    marginTop: -25,
    textAlign: 'center',
    fontSize: '2rem / 1.6',
  },
})