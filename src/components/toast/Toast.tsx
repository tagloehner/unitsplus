import React from 'react'
import { styles } from './styles'
import { Animated, Text } from 'react-native'
import LottieView from 'lottie-react-native'
import { ToastProps } from './interfaces'


export const Toast: React.FC<ToastProps> = ({ themeStyles, message, animatedValue, onRef, listHeight }) => {
  const { animatedView, contentText } = styles
  const { container, content } = themeStyles
  return (
    <Animated.View
      pointerEvents="none"
      style={{
        ...animatedView,
        ...container,
        top: (listHeight / 2 - 70),
        opacity: animatedValue
      }}
    >
      <LottieView
        ref={onRef}
        source={require('../../animations/data.json')}
        colorFilters={[{
          keypath: "Outlines",
          color: content.color
        }]}
        loop={false}
        style={{ height: 120, width: 120 }}
      />
      <Text style={{ ...contentText, ...content }}>
        {message}
      </Text>
    </Animated.View>
  )
}
