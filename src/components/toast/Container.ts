import { connect } from 'react-redux'
import { toastStyles, listHeight } from '../../redux/selectors'
import { compose, withProps, withState, lifecycle, withHandlers } from 'recompose'
import { Toast } from './Toast'
import { Animated } from 'react-native'
import { ToastProps, ToastStateProps } from './interfaces'


const mapStateToProps = state => {
  const { message, duration, id } = state.toast
  return ({
    message,
    duration,
    id,
    themeStyles: toastStyles(state),
    listHeight: listHeight(state)
  })
}

export const Container = compose(
  connect<ToastStateProps>(mapStateToProps),
  withHandlers(() => {
    let animations = null;

    return {
      onRef: () => (ref: any) => (animations = ref),
      play: () => (params: any) => animations.play(params),
      reset: () => () => animations.reset()
    }
  }),
  withState('modalShown', 'setModalShown', false),
  withState('animatedValue', 'setAnimatedValue', new Animated.Value(0)),
  withProps(({ setModalShown, animatedValue, duration }) => ({
    closeToast: () => {
      setTimeout(() => {
        setModalShown(false)
        Animated.timing(
          animatedValue,
          {
            toValue: 0,
            duration: 400,
          }).start()
      }, duration)
    },
  })),
  withProps(({ setModalShown, animatedValue, modalShown, closeToast }) => ({
    callToast: () => {
      if (modalShown) return
      setModalShown(true)
      Animated.timing(
        animatedValue,
        {
          toValue: 1,
          duration: 200,
        }).start(closeToast())
    }
  })),
  lifecycle<ToastProps, ToastStateProps>({
    componentDidUpdate(prevProps) {
      const { id, callToast } = this.props
      const didToastIdChange = id !== prevProps.id
      didToastIdChange && callToast()
      const { reset, play, modalShown } = this.props
      if (modalShown)
      {
        reset()
        play()
      }
    },
  })
)(Toast)