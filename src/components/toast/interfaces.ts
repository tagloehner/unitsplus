import { ThemeToast } from '../../themes/interfaces'

export interface ToastProps extends ToastStateProps, ContainerInnerProps { }

export interface ToastStateProps {
  message: string
  duration: number
  id: string
  themeStyles: ThemeToast
  listHeight: number
}

export interface ContainerInnerProps {
  animatedValue: any
  onRef: (props: unknown) => (ref: any) => any
  play: (props?: unknown) => (params: any) => any,
  reset: () => () => any,
  callToast: () => void
  closeToast: () => void
  modalShown: boolean
}