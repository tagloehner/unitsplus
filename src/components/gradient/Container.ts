import { connect } from 'react-redux'
import { gradientStyles } from '../../redux/selectors'
import { Gradient } from './Gradient'
import { GradientStateProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: gradientStyles(state),
})

export const Container = connect<GradientStateProps>(mapStateToProps)(Gradient)
