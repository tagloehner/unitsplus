import { ThemeGradient } from '../../themes/interfaces'


export interface GradientProps extends GradientStateProps, ContainerOutterProps { }

export interface GradientStateProps {
  themeStyles: ThemeGradient
}

export interface ContainerOutterProps {
  position: 'bottom' | 'top'
}