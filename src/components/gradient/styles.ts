import { StyleSheet } from 'react-native'


export const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    right: 0,
    left: 0,
    height: 3,
    zIndex: 1,
  },
})