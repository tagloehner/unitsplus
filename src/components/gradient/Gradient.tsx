import React from 'react'
import LinearGradient from 'react-native-linear-gradient'
import { View } from 'react-native'
import { GradientProps } from './interfaces'
import { styles } from './styles'

export const Gradient: React.FC<GradientProps> = ({ position, themeStyles }) => {
  const { topGradientColors, bottomGradientColors } = themeStyles
  const { mainContainer } = styles

  const topPosition = {
    ...mainContainer,
    top: 0,
  }

  const bottomPosition = {
    ...mainContainer,
    bottom: 0,
  }

  return (
    <LinearGradient
      colors={position === 'top' ? topGradientColors : bottomGradientColors}
      pointerEvents="none"
      style={position === 'top' ? topPosition : bottomPosition}
    >
      <View style={{ flex: 1, backgroundColor: 'transparent' }} />
    </LinearGradient>
  )
}