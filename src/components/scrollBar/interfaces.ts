import { ThemeScrollBar, ThemeScrollBarItem } from '../../themes/interfaces'
import { i18nCategories } from '../../i18n/interfaces'


export interface ScrollBarProps extends ScrollBarStateProps, ScrollBarActionProps { }

export interface ScrollBarStateProps {
  selectedIndex: number
  themeStyles: ThemeScrollBar
  i18n: i18nCategories
  scrollBarHeight: number
}

export interface ScrollBarActionProps {
  onScrollBarButtonPressed: ({ category: string, index: number }) => void
}

export interface ScrollBarItemProps {
  themeStyles: ThemeScrollBarItem
  onPress: () => void
  title: string
  disabled: boolean
  scrollBarHeight: number
}
