import React from 'react'
import { scrollBarItemStyles as styles } from './styles'
import { Button } from '../button'
import { ScrollBarItemProps } from './interfaces'


export const ScrollBarItem: React.FC<ScrollBarItemProps> = ({ themeStyles, onPress, title, disabled, scrollBarHeight }) => {
  const { mainContainer, fontRem: { fontSize } } = styles
  const {
    container: {
      backgroundColor,
      borderColor,
      borderWidth,
    },
    text: {
      color,
    },
  } = themeStyles

  const customPadding = scrollBarHeight / 3

  const containerStyle = {
    ...mainContainer,
    paddingRight: customPadding,
    paddingLeft: customPadding,
    borderWidth,
    backgroundColor: disabled ? backgroundColor.disabled : backgroundColor.enabled,
    borderColor: disabled ? borderColor.disabled : borderColor.enabled,
  }

  const textStyle = {
    color: disabled ? color.disabled : color.enabled,
    fontSize,
  }

  return (
    <Button
      textStyle={textStyle}
      title={title}
      onPress={onPress}
      disabled={disabled}
      containerStyle={containerStyle}
      activeOpacity={1}
    />
  )
}
