import EStyleSheet from 'react-native-extended-stylesheet'


export const scrollBarItemStyles = EStyleSheet.create({
  mainContainer: {
    borderRadius: '1rem / 2.5',
    flex: 1,
    margin: '1rem / 6',
  },
  fontRem: {
    fontSize: '1.2rem',
  },
})


export const scrollBarStyles = EStyleSheet.create({
  mainContainer: {
    paddingLeft: '1rem / 8',
    paddingRight: '1rem / 8',
  }
})