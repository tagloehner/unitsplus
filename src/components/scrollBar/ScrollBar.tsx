import React from 'react'
import { FlatList, View } from 'react-native'
import { ScrollBarItem } from './ScrollBarItem'
import { operationsList } from '../../models'
import { ScrollBarProps } from './interfaces'
import { scrollBarStyles as styles } from './styles'

export const ScrollBar: React.FC<ScrollBarProps> = ({
  onScrollBarButtonPressed,
  selectedIndex,
  themeStyles,
  i18n,
  scrollBarHeight
}) => {
  const { backgroundColor } = themeStyles
  const { mainContainer } = styles
  return (
    <View style={{ ...mainContainer, backgroundColor, height: scrollBarHeight }}>
      <FlatList
        data={operationsList}
        horizontal
        extraData={{ selectedIndex, i18n }}
        renderItem={({ item, index }) => {
          const { type } = item
          const disabled = index === selectedIndex
          return (
            <ScrollBarItem
              scrollBarHeight={scrollBarHeight}
              title={i18n[type]}
              disabled={disabled}
              themeStyles={themeStyles.item}
              onPress={() => onScrollBarButtonPressed({ category: type, index })}
            />
          )
        }}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_, index) => index.toString()}
      />
    </View>
  )
}
