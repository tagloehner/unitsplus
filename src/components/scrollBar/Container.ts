import { compose } from 'recompose'
import { connect } from 'react-redux'
import { scrollBarStyles, i18nCategories, scrollBarHeight } from '../../redux/selectors'
import { ScrollBar } from './ScrollBar'
import { onScrollBarButtonPressed } from '../../redux/actionCreators'
import { ScrollBarStateProps, ScrollBarActionProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: scrollBarStyles(state),
  selectedIndex: state.scrollBar.index,
  i18n: i18nCategories(state),
  scrollBarHeight: scrollBarHeight(state),
})

export const Container = compose(
  connect<ScrollBarStateProps, ScrollBarActionProps>(mapStateToProps, { onScrollBarButtonPressed }),
)(ScrollBar)
