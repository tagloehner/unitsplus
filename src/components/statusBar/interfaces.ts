import { ThemeStatusBar } from '../../themes/interfaces'

export interface StatusBarProps extends StatusBarStateProps { }

export interface StatusBarStateProps {
  themeStyles: ThemeStatusBar
}