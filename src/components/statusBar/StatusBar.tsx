import React from 'react'
import { StatusBarProps } from './interfaces'
import { StatusBar as ReactNativeStatusBar } from 'react-native'


export const StatusBar: React.FC<StatusBarProps> = ({ themeStyles }) => {
  const { contentStyle } = themeStyles
  return (
    <ReactNativeStatusBar
      barStyle={contentStyle}
    />
  )
}