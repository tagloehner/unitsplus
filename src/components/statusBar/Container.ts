import { connect } from 'react-redux'
import { statusBarStyles } from '../../redux/selectors'
import { StatusBar } from './StatusBar'
import { StatusBarStateProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: statusBarStyles(state),
})

export const Container = connect<StatusBarStateProps>(mapStateToProps)(StatusBar)
