import { connect } from 'react-redux'
import { displayStyles, listItemHeight } from '../../redux/selectors'
import { Display } from './Display'
import { DisplayStateProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: displayStyles(state),
  listItemHeight: listItemHeight(state),
})

export const Container = connect<DisplayStateProps>(mapStateToProps)(Display)
