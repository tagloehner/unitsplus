
import EStyleSheet from 'react-native-extended-stylesheet'
import { Dimensions } from 'react-native'


export const styles = EStyleSheet.create({
  mainContainer: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    flexDirection: 'row',
  },
  titlePosition: {
    position: 'absolute',
    fontSize: '1rem',
    left: '1rem / 2',
    top: '1rem / 4',
  },
  subtitlePosition: {
    fontSize: '0.8rem',
    position: 'absolute',
    right: '1rem / 2',
    bottom: '1rem / 4',
  },
  valuePosition: {
    textAlign: 'right',
    justifyContent: 'center',
    paddingRight: '1rem / 4',
    paddingLeft: '1rem / 4',
  },
})

