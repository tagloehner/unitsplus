import { ThemeDisplay } from '../../themes/interfaces'

export interface DisplayProps extends DisplayStateProps, ContainerOutterProps, ContainerInnerProps {

}

export interface DisplayStateProps {
  themeStyles: ThemeDisplay
  listItemHeight: number
}

export interface ContainerOutterProps {
  leftTitle: string
  rightTitle: string
  leftSubtitle: string
  rightSubtitle: string
  leftValue: string
  rightValue: string
  listItemHeight: number
}

export interface ContainerInnerProps {

}