import React from 'react'
import { View, Text } from 'react-native'
import { DisplayProps } from './interfaces'
import { styles } from './styles'


export const Display: React.FC<DisplayProps> = ({
  leftTitle,
  rightTitle,
  leftSubtitle,
  rightSubtitle,
  leftValue,
  rightValue,
  themeStyles,
  listItemHeight,
}) => {
  const { leftDisplay, rightDisplay } = themeStyles
  const { mainContainer, titlePosition, subtitlePosition, valuePosition } = styles

  return (
    <View style={{ height: listItemHeight + 3, ...mainContainer }} pointerEvents="none">
      <View style={{ ...leftDisplay.container, ...valuePosition }}>
        <Text style={{ ...leftDisplay.title, ...titlePosition }} numberOfLines={1}>
          {leftTitle}
        </Text>
        <Text style={{ ...leftDisplay.value, ...valuePosition }} adjustsFontSizeToFit numberOfLines={1} minimumFontScale={0.5}>
          {leftValue}
        </Text>
        <Text style={{ ...leftDisplay.subtitle, ...subtitlePosition }} numberOfLines={1}>
          {leftSubtitle}
        </Text>
      </View>
      <View style={{ ...rightDisplay.container, ...valuePosition }}>
        <Text style={{ ...rightDisplay.title, ...titlePosition }} numberOfLines={1}>
          {rightTitle}
        </Text>
        <Text style={{ ...rightDisplay.value, ...valuePosition }} adjustsFontSizeToFit numberOfLines={1} minimumFontScale={0.5}>
          {rightValue}
        </Text>
        <Text style={{ ...rightDisplay.subtitle, ...subtitlePosition }}>
          {rightSubtitle}
        </Text>
      </View>
    </View>
  )
}
