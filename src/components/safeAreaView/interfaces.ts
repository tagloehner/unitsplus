import { ThemeSafeAreaView } from '../../themes/interfaces'

export interface SafeAreaViewProps extends SafeAreaViewStateProps {

}
export interface SafeAreaViewStateProps {
  themeStyles: ThemeSafeAreaView
}