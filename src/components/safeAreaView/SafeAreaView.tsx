
import React from 'react'
import { SafeAreaViewProps } from './interfaces'
import { SafeAreaView as SafeArea } from 'react-native'


export const SafeAreaView: React.FC<SafeAreaViewProps> = ({ themeStyles, children }) => (
  <SafeArea style={{ flex: 1, ...themeStyles }}>
    {children}
  </SafeArea>
)
