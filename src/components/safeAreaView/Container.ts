import { connect } from 'react-redux'
import { safeAreaViewStyles } from '../../redux/selectors'
import { SafeAreaView } from './SafeAreaView'
import { SafeAreaViewStateProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: safeAreaViewStyles(state),
})

export const Container = connect<SafeAreaViewStateProps>(mapStateToProps)(SafeAreaView)
