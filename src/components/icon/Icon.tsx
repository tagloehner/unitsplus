import React from 'react'
import { Icon as NativeBaseIcon } from 'native-base'
import { IconProps } from './interfaces'


export const Icon: React.FC<IconProps> = ({ name, size = 24, color = 'white', type }) =>
  <NativeBaseIcon name={name} style={{ fontSize: size, color }} type={type} />
