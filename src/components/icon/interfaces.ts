export interface IconProps {
  name: string,
  size?: number,
  color?: string,
  type: TypeIconType,
}

type TypeIconType = 'EvilIcons' | 'Feather' | 'FontAwesome' | 'FontAwesome5' | 'Foundation' | 'Ionicons' | 'MaterialCommunityIcons' | 'MaterialIcons' | 'Octicons' | 'SimpleLineIcons' | 'Zocial'
