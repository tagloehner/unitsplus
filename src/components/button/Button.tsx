import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Icon } from '../icon'
import { ButtonProps } from './interfaces'
import { styles } from './styles'


export const Button: React.FC<ButtonProps> = ({
  title,
  onPress,
  containerStyle,
  textStyle,
  themeStyles,
  activeOpacity,
  iconProps = { name: undefined, type: undefined, color: undefined },
  disabled,
}) => {
  const renderTitleOrIcon = () => {
    const { name, type, size } = iconProps
    return title
      ? <Text style={textStyle} adjustsFontSizeToFit numberOfLines={1}>{title}</Text>
      : <Icon name={name} type={type} size={size} />
  }
  const { buttonContainer, touchableOpacity } = styles
  return (
    <TouchableOpacity
      style={{ ...touchableOpacity }}
      onPress={onPress}
      activeOpacity={activeOpacity}
      disabled={disabled}
    >
      <View
        style={{
          ...buttonContainer,
          ...themeStyles,
          ...containerStyle,
        }}
      >
        {renderTitleOrIcon()}
      </View>
    </TouchableOpacity>
  )
}
