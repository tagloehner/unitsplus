import { ThemeButton } from '../../themes/interfaces'
import {
  TextStyle,
  ViewStyle,
  TouchableOpacityProps
} from 'react-native'
import { IconProps } from '../icon/interfaces'


export interface ButtonProps extends TouchableOpacityProps, ButtonStateProps {
  title: string
  containerStyle: ViewStyle
  textStyle: TextStyle
  iconProps?: IconProps
}

export interface ButtonStateProps {
  themeStyles: ThemeButton
}

export interface StylesProps {
  touchableOpacity: TouchableOpacityProps
  buttonContainer: ViewStyle
}