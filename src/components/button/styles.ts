import { StyleSheet } from 'react-native'
import { StylesProps } from './interfaces'

export const styles = StyleSheet.create<StylesProps>({
  touchableOpacity: {
    flex: 1,
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
})
