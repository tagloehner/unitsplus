import { connect } from 'react-redux'
import { buttonStyles } from '../../redux/selectors'
import { Button } from './Button'
import { ButtonStateProps } from './interfaces'


const mapStateToProps = state => ({
  themeStyles: buttonStyles(state),
})

export const Container = connect<ButtonStateProps>(mapStateToProps)(Button)
