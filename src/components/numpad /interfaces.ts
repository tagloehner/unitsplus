import { ThemeNumpad, ThemeNumpadItem } from '../../themes/interfaces'
import { i18nNumpad } from '../../i18n/interfaces'
import { GestureResponderEvent, TextStyle, ViewStyle } from 'react-native'
import { IconProps } from '../icon/interfaces'


export interface NumpadProps extends NumpadStateProps, NumpadActionProps, ContainerOutterProps { }

export interface NumpadStateProps {
  clipboardValue: string
  themeStyles: ThemeNumpad
  numpadItemHeight: number
  i18n: i18nNumpad,
  copyToClipBoard: (clipboardValue: any) => Promise<void>
}

export interface NumpadActionProps {
  onNumberPressed: (payload: any) => (dispatch: any) => any
  onBackspacePressed: (payload: any) => (dispatch: any) => any
  onAllClearPressed: (payload: any) => (dispatch: any) => any
  onMinusPLusPress: (payload: any) => (dispatch: any) => any
  onDotPressed: (payload: any) => (dispatch: any) => any
  callToast: (payload: any) => (dispatch: any) => any
}

export interface NumpadItemProps {
  title: string
  themeStyles: ThemeNumpadItem
  onPress: (event: GestureResponderEvent) => void
  iconProps: IconProps
  containerStyle: ViewStyle
  textStyle: TextStyle
}

export interface ContainerOutterProps {
  category: string
}