import EStyleSheet from 'react-native-extended-stylesheet'


export const numpadStyles = EStyleSheet.create({
  textRem: {
    fontSize: '1.4rem',
  },
})

export const numpadItemStyles = EStyleSheet.create({
  mainContainer: {
    borderWidth: '1rem / 32',
  },
  fontRem: {
    fontSize: '1.2rem',
  },
})
