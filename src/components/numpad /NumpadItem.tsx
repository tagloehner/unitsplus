import React from 'react'
import { numpadItemStyles as styles } from './styles'
import { Button } from '../button'
import { NumpadItemProps } from './interfaces'


export const NumpadItem: React.FC<NumpadItemProps> = ({
  title,
  themeStyles,
  onPress,
  iconProps,
  containerStyle,
  textStyle,
}) => {
  const {
    standard: { borderColor, container, text },
  } = themeStyles
  const { mainContainer, fontRem: { fontSize } } = styles

  return (
    <Button
      title={title}
      containerStyle={{
        ...mainContainer,
        ...container,
        ...containerStyle,
        borderColor,
      }}
      onPress={onPress}
      iconProps={iconProps}
      textStyle={{ fontSize, ...text, ...textStyle }}
      activeOpacity={0.6}
    />
  )
}


