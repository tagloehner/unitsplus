import { connect } from 'react-redux'
import { copyToClipBoard, toLocaleString } from '../../functions'
import {
  onNumberPressed,
  onBackspacePressed,
  onAllClearPressed,
  onMinusPLusPress,
  onDotPressed,
  callToast,
} from '../../redux/actionCreators'
import {
  numpadStyles,
  makeGetCalculation,
  numpadItemHeight,
  i18nNumpad,
} from '../../redux/selectors'
import { Numpad } from './Numpad'
import { NumpadStateProps, NumpadActionProps } from './interfaces'


const mmakeMapStateToProps = () => {
  const getCalculationResult = makeGetCalculation()
  const mapStateToProps = (state, props) => {
    const { result } = getCalculationResult(state, props)
    return ({
      clipboardValue: toLocaleString(result),
      themeStyles: numpadStyles(state),
      numpadItemHeight: numpadItemHeight(state),
      i18n: i18nNumpad(state),
      copyToClipBoard,
    })
  }
  return mapStateToProps
}

const mapDispatchToProps = {
  onNumberPressed,
  onBackspacePressed,
  onAllClearPressed,
  onMinusPLusPress,
  onDotPressed,
  callToast,
}

export const Container = connect<NumpadStateProps, NumpadActionProps>(mmakeMapStateToProps, mapDispatchToProps)(Numpad)
