import React from 'react'
import { FlatList, View } from 'react-native'
import { NumpadItem } from './NumpadItem'
import model from './model'
import { NavigationService } from '../../navigator'
import { switchCase } from '../../functions'
import { MESSAGE_COPY, AC } from '../../constants'
import { numpadStyles as styles } from './styles'
import { NumpadProps } from './interfaces'


export const Numpad: React.FC<NumpadProps> = ({
  numpadItemHeight,
  onBackspacePressed,
  onNumberPressed,
  onAllClearPressed,
  onMinusPLusPress,
  onDotPressed,
  copyToClipBoard,
  clipboardValue,
  themeStyles,
  callToast,
  category,
  i18n,
}) => {
  const {
    backgroundColor,
    item: {
      backspace,
      number,
      allClear,
      minusPlus,
      copy,
      standard,
      preferences,
    },
  } = themeStyles

  const { textRem: { fontSize } } = styles

  const renderItem = item => {
    const { type, value } = item

    const config = switchCase({
      number: {
        title: Number.isInteger(value) && value.toString(),
        onPress: () => onNumberPressed({ input: value, category }),
        containerStyle: { ...number.container },
        textStyle: { ...number.text, fontSize },
      },
      backspace: {
        onPress: () => onBackspacePressed({ category }),
        iconProps: { ...backspace.icon },
        containerStyle: { ...backspace.container },
      },
      allClear: {
        onPress: () => onAllClearPressed({ category }),
        title: i18n[AC],
        containerStyle: { ...allClear.container },
        textStyle: { ...allClear.text, fontSize },
      },
      minusPlus: {
        onPress: () => onMinusPLusPress({ category }),
        title: '-/+',
        containerStyle: { ...minusPlus.container },
        textStyle: { ...minusPlus.text, fontSize },
      },
      dot: {
        onPress: () => onDotPressed({ input: value, category }),
        title: '.',
        containerStyle: { ...standard.container },
        textStyle: { ...standard.text, fontSize },
      },
      copy: {
        onPress: () => {
          copyToClipBoard(clipboardValue)
          callToast({ message: i18n[MESSAGE_COPY], duration: 800 })
        },
        iconProps: { ...copy.icon },
        containerStyle: { ...copy.container },
      },
      preferences: {
        onPress: () => NavigationService.navigate('Preferences', null),
        title: '',
        containerStyle: { ...preferences.container },
        iconProps: { ...preferences.icon },
      },
    })({})(type)

    const { title, containerStyle, iconProps, onPress, textStyle } = config

    return (
      <NumpadItem
        title={title}
        onPress={onPress}
        containerStyle={{ ...containerStyle, height: numpadItemHeight }}
        iconProps={iconProps}
        textStyle={textStyle}
        themeStyles={themeStyles.item}
      />
    )
  }

  return (
    <View>
      <FlatList
        style={{ backgroundColor }}
        data={model}
        bounces={false}
        numColumns={4}
        renderItem={({ item }) => renderItem(item)}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_, itemIndex) => itemIndex.toString()}
      />
    </View>
  )
}