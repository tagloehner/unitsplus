import { switchCase } from '../functions'
import { CELSIUS, FAHRENHEIT, KELVIN, RANKINE } from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInCelsius = switchCase({
    [CELSIUS]: value * 1,
    [FAHRENHEIT]: ((value - 32) * 5) / 9,
    [KELVIN]: value - 273.15,
    [RANKINE]: ((value - 491.67) * 5) / 9
  })(0)(leftOperation)

  return switchCase({
    [CELSIUS]: inputValueInCelsius / 1,
    [FAHRENHEIT]: inputValueInCelsius * (9 / 5) + 32,
    [KELVIN]: inputValueInCelsius + 273.15,
    [RANKINE]: inputValueInCelsius * (9 / 5) + 491.67
  })(0)(rightOperation)
}
