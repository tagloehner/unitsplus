import { switchCase } from '../functions'
import {
  BTU_H,
  HORSE_POWER,
  KILOWATT,
  WATT,
  TON_OF_REFRIGERATION
} from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInWatts = switchCase({
    [BTU_H]: value * 3.412141633,
    [HORSE_POWER]: value * 745.699872,
    [KILOWATT]: value * 1000,
    [WATT]: value * 1,
    [TON_OF_REFRIGERATION]: value / 0.00028434513626109
  })(0)(leftOperation)

  return switchCase({
    [BTU_H]: inputValueInWatts / 3.412141633,
    [HORSE_POWER]: inputValueInWatts / 745.699872,
    [KILOWATT]: inputValueInWatts / 1000,
    [WATT]: inputValueInWatts / 1,
    [TON_OF_REFRIGERATION]: inputValueInWatts * 0.00028434513626109
  })(0)(rightOperation)
}
