import { switchCase } from '../functions'
import {
  ATMOSPHERE,
  BAR,
  MILLIBAR,
  INCH_MERCURY,
  MILLIMETER_MERCURY,
  MEGA_PASCAL,
  KILO_PASCAL,
  PASCAL_PA,
  POUND_PSF,
  POUND_PSI,
} from '../constants'


export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInBar = switchCase({
    [ATMOSPHERE]: value / 0.98692326671,
    [BAR]: value * 1,
    [MILLIBAR]: value / 1000,
    [INCH_MERCURY]: value / 29.529980164712,
    [MILLIMETER_MERCURY]: value / 750.06157584566,
    [MEGA_PASCAL]: value * 10,
    [KILO_PASCAL]: value / 100,
    [PASCAL_PA]: value / 100000,
    [POUND_PSF]: value / 2088.5434304802,
    [POUND_PSI]: value / 14.503773800722,
  })(0)(leftOperation)

  return switchCase({
    [ATMOSPHERE]: inputValueInBar * 0.98692326671,
    [BAR]: inputValueInBar / 1,
    [MILLIBAR]: inputValueInBar * 1000,
    [INCH_MERCURY]: inputValueInBar * 29.529980164712,
    [MILLIMETER_MERCURY]: inputValueInBar * 750.06157584566,
    [MEGA_PASCAL]: inputValueInBar / 10,
    [KILO_PASCAL]: inputValueInBar * 100,
    [PASCAL_PA]: inputValueInBar * 100000,
    [POUND_PSF]: inputValueInBar * 2088.5434304802,
    [POUND_PSI]: inputValueInBar * 14.503773800722,
  })(0)(rightOperation)
}
