import { switchCase } from '../functions'
import * as CONSTANTS from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInMeters = switchCase({
    [CONSTANTS.KILOMETER]: value * 1000,
    [CONSTANTS.METER]: value * 1,
    [CONSTANTS.DECIMETER]: value / 10,
    [CONSTANTS.CENTIMER]: value / 100,
    [CONSTANTS.MILLIMETER]: value / 1000,
    [CONSTANTS.MILE]: value * 1609.344,
    [CONSTANTS.YARD]: value / 1.0936133,
    [CONSTANTS.FEET]: value * 0.3048,
    [CONSTANTS.INCH]: value / 39.370078740157,
    [CONSTANTS.NAUTICAL_MILE]: value * 1852,
    [CONSTANTS.LEAGUE]: value * 5556,
    [CONSTANTS.FURLONG]: value / 0.0049709695378987,
    [CONSTANTS.ROD]: value / 0.19883878152,
    [CONSTANTS.CHAIN]: value / 0.049709595959596
  })(0)(leftOperation)

  return switchCase({
    [CONSTANTS.KILOMETER]: inputValueInMeters / 1000,
    [CONSTANTS.METER]: inputValueInMeters,
    [CONSTANTS.DECIMETER]: inputValueInMeters * 10,
    [CONSTANTS.CENTIMER]: inputValueInMeters * 100,
    [CONSTANTS.MILLIMETER]: inputValueInMeters * 1000,
    [CONSTANTS.MILE]: inputValueInMeters / 1609.344,
    [CONSTANTS.YARD]: inputValueInMeters * 1.0936133,
    [CONSTANTS.FEET]: inputValueInMeters / 0.3048,
    [CONSTANTS.INCH]: inputValueInMeters * 39.370078740157,
    [CONSTANTS.NAUTICAL_MILE]: inputValueInMeters / 1852,
    [CONSTANTS.LEAGUE]: inputValueInMeters / 5556,
    [CONSTANTS.FURLONG]: inputValueInMeters * 0.0049709695378987,
    [CONSTANTS.ROD]: inputValueInMeters * 0.19883878152,
    [CONSTANTS.CHAIN]: inputValueInMeters * 0.049709595959596
  })(0)(rightOperation)
}
