import { switchCase } from '../functions'
import {
  HECTARE,
  ARE,
  SQUARE_KILOMETER,
  SQUARE_METER,
  SQUARE_DECIMETER,
  SQUARE_CENTIMETER,
  SQUARE_MILLIMETER,
  SQUARE_MILE,
  ACRE,
  SQUARE_YARD,
  SQUARE_FOOT,
  SQUARE_INCH
} from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInSquareMeter = switchCase({
    [HECTARE]: value * 10000,
    [ARE]: value * 100,
    [SQUARE_KILOMETER]: value * 1e6,
    [SQUARE_METER]: value * 1,
    [SQUARE_DECIMETER]: value / 100,
    [SQUARE_CENTIMETER]: value / 10000,
    [SQUARE_MILLIMETER]: value / 1e6,
    [SQUARE_MILE]: value * 2.59e6,
    [ACRE]: value * 4046.8564224,
    [SQUARE_YARD]: value * 0.83612736,
    [SQUARE_FOOT]: value / 10.763910417,
    [SQUARE_INCH]: value * 0.00064516,
  })(0)(leftOperation)

  return switchCase({
    [HECTARE]: inputValueInSquareMeter / 10000,
    [ARE]: inputValueInSquareMeter / 100,
    [SQUARE_KILOMETER]: inputValueInSquareMeter / 1e6,
    [SQUARE_METER]: inputValueInSquareMeter / 1,
    [SQUARE_DECIMETER]: inputValueInSquareMeter * 100,
    [SQUARE_CENTIMETER]: inputValueInSquareMeter * 10000,
    [SQUARE_MILLIMETER]: inputValueInSquareMeter * 1e6,
    [SQUARE_MILE]: inputValueInSquareMeter / 2.59e6,
    [ACRE]: inputValueInSquareMeter / 4046.8564224,
    [SQUARE_YARD]: inputValueInSquareMeter / 0.83612736,
    [SQUARE_FOOT]: inputValueInSquareMeter * 10.76391041671,
    [SQUARE_INCH]: inputValueInSquareMeter / 0.00064516,
  })(0)(rightOperation)
}
