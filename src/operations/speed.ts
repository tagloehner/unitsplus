import { switchCase } from '../functions'
import {
  KILOMETER_PER_HOUR,
  METER_PER_SECOND,
  MILE_PER_HOUR,
  FEET_PER_MINUTE,
  FEET_PER_SECOND,
  KNOT,
  SPEED_OF_LIGHT,
  SPEED_OF_SOUND
} from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInKilometerPerHour = switchCase({
    [KILOMETER_PER_HOUR]: value * 1,
    [METER_PER_SECOND]: value * 3.6,
    [MILE_PER_HOUR]: value / 0.621371192,
    [FEET_PER_MINUTE]: value / 54.680664916885,
    [FEET_PER_SECOND]: value / 3280.8398950131,
    [KNOT]: value / 0.53995680345572,
    [SPEED_OF_LIGHT]: value * 1079252848.7999,
    [SPEED_OF_SOUND]: value * 1234.8
  })(0)(leftOperation)

  return switchCase({
    [KILOMETER_PER_HOUR]: inputValueInKilometerPerHour / 1,
    [METER_PER_SECOND]: inputValueInKilometerPerHour / 3.6,
    [MILE_PER_HOUR]: inputValueInKilometerPerHour * 0.621371192,
    [FEET_PER_MINUTE]: inputValueInKilometerPerHour * 54.680664916885,
    [FEET_PER_SECOND]: inputValueInKilometerPerHour * 3280.8398950131,
    [KNOT]: inputValueInKilometerPerHour * 0.53995680345572,
    [SPEED_OF_LIGHT]: inputValueInKilometerPerHour / 1079252848.7999,
    [SPEED_OF_SOUND]: inputValueInKilometerPerHour / 1234.8
  })(0)(rightOperation)
}
