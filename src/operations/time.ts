import { switchCase } from '../functions'
import {
  MILLENNIUM,
  CENTURY,
  DECADE,
  CALENDAR_YEAR,
  MONTH,
  WEEK,
  DAY,
  HOUR,
  MINUTE,
  SECOND,
  MILLISECOND
} from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueMinutes = switchCase({
    [MILLENNIUM]: value * 5.256e8,
    [CENTURY]: value * 5.256e7,
    [DECADE]: value * 5.256e6,
    [CALENDAR_YEAR]: value * 525600,
    [MONTH]: value * 43800.048,
    [WEEK]: value * 10080,
    [DAY]: value * 1440,
    [HOUR]: value * 60,
    [MINUTE]: value * 1,
    [SECOND]: value / 60,
    [MILLISECOND]: value / 60000
  })(0)(leftOperation)

  return switchCase({
    [MILLENNIUM]: inputValueMinutes / 5.256e8,
    [CENTURY]: inputValueMinutes / 5.256e7,
    [DECADE]: inputValueMinutes / 5.256e6,
    [CALENDAR_YEAR]: inputValueMinutes / 525600,
    [MONTH]: inputValueMinutes / 43800.048,
    [WEEK]: inputValueMinutes / 10080,
    [DAY]: inputValueMinutes / 1440,
    [HOUR]: inputValueMinutes / 60,
    [MINUTE]: inputValueMinutes / 1,
    [SECOND]: inputValueMinutes * 60,
    [MILLISECOND]: inputValueMinutes * 60000
  })(0)(rightOperation)
}
