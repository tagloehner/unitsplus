import { switchCase } from '../functions'
import * as CONSTANTS from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInGrams = switchCase({
    [CONSTANTS.KILOGRAM]: value * 1000,
    [CONSTANTS.GRAM]: value / 1,
    [CONSTANTS.MILLIGRAM]: value / 1000,
    [CONSTANTS.POUND]: value * 453.59237,
    [CONSTANTS.OUNCE]: value / 0.03527396195,
    [CONSTANTS.TON_METRIC]: value * 1e+6,
    [CONSTANTS.TON_LONG]: value / 0.00000098421,
    [CONSTANTS.TON_SHORT]: value * 907184.74,
    [CONSTANTS.STONE]: value * 6350.29318
  })(0)(leftOperation)

  return switchCase({
    [CONSTANTS.KILOGRAM]: inputValueInGrams / 1000,
    [CONSTANTS.GRAM]: inputValueInGrams * 1,
    [CONSTANTS.MILLIGRAM]: inputValueInGrams * 1000,
    [CONSTANTS.POUND]: inputValueInGrams / 453.59237,
    [CONSTANTS.OUNCE]: inputValueInGrams * 0.03527396195,
    [CONSTANTS.TON_METRIC]: inputValueInGrams / 1e+6,
    [CONSTANTS.TON_LONG]: inputValueInGrams * 0.00000098421,
    [CONSTANTS.TON_SHORT]: inputValueInGrams / 907184.74,
    [CONSTANTS.STONE]: inputValueInGrams / 6350.29318
  })(0)(rightOperation)
}
