import { switchCase } from '../functions'
import { BIT, BYTE, KILOBYTE, GIGABYTE, TERABYTE, MEGABYTE } from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInBytes = switchCase({
    [BIT]: value / 8,
    [BYTE]: value * 1,
    [KILOBYTE]: value * 1024,
    [MEGABYTE]: value * 1048576,
    [GIGABYTE]: value * 1073741824,
    [TERABYTE]: value * 1099511627776
  })(0)(leftOperation)

  return switchCase({
    [BIT]: inputValueInBytes * 8,
    [BYTE]: inputValueInBytes / 1,
    [KILOBYTE]: inputValueInBytes / 1024,
    [MEGABYTE]: inputValueInBytes / 1048576,
    [GIGABYTE]: inputValueInBytes / 1073741824,
    [TERABYTE]: inputValueInBytes / 1099511627776
  })(0)(rightOperation)
}
