import { switchCase } from '../functions'
import {
  MILES_GALLON_US,
  MILES_GALLON_UK,
  LITERS_PER_ONE_HUNDRED_KM,
  LITERS_PER_KM,
  KILOMETERS_PER_LITER
} from '../constants'

export default ({ value, leftOperation, rightOperation }) => {
  const inputValueInKmPerLiter = switchCase({
    [MILES_GALLON_US]: value * 0.425144,
    [MILES_GALLON_UK]: value / 2.824809363318222,
    [LITERS_PER_ONE_HUNDRED_KM]: 100 / (value / 100),
    [LITERS_PER_KM]: 1 / value,
    [KILOMETERS_PER_LITER]: value * 1
  })(0)(leftOperation)

  return switchCase({
    [MILES_GALLON_US]: inputValueInKmPerLiter / 0.425144,
    [MILES_GALLON_UK]: inputValueInKmPerLiter * 2.824809363318222,
    [LITERS_PER_ONE_HUNDRED_KM]: 100 / inputValueInKmPerLiter,
    [LITERS_PER_KM]: 1 * inputValueInKmPerLiter,
    [KILOMETERS_PER_LITER]: inputValueInKmPerLiter / 1
  })(0)(rightOperation)
}
