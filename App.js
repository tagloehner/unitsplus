import React from 'react'
import EStyleSheet from 'react-native-extended-stylesheet'
import { Core } from './src/core'
import { YellowBox } from 'react-native'


EStyleSheet.build({})

YellowBox.ignoreWarnings([
  'RCTRootView cancelTouches'
]);

export const App = () => <Core />